%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt]{article}



\usepackage[utf8]{inputenc} % Required for inputting international characters

\usepackage[T1]{fontenc} % Output font encoding for international characters

\usepackage[top=2cm,right=2cm, left=2cm]{geometry} % set margins

\usepackage{setspace} % paragraph spacing

\usepackage{mathpazo} % Palatino font


\usepackage{hyperref}  % for hyperlinks to resources\

\usepackage{graphicx} % for logo

\usepackage{caption}

\usepackage{tabularx} % in the preamble
\newcolumntype{Y}{>{\centering\arraybackslash}X}
\newcolumntype{b}{>{\hsize=.5\hsize}X} % column 1 = 50% of a column
\newcolumntype{s}{>{\hsize=.4\hsize}X} % column 2 = 40% of a column
\newcolumntype{m}{>{\hsize=1.1\hsize}X} % column 3 = 110% of a column

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for horizontal lines, change thickness here
	
	\center % Centre everything on the page
	
	%------------------------------------------------
	%	Headings
	%------------------------------------------------
	
	\textsc{\LARGE University of Regina}\\[1.5cm] % Main heading such as the name of your university/college
	
	\textsc{\Large ENSE 481: Embedded Systems and Co-design}\\[0.5cm] % Major heading such as course name
	
	\textsc{\large Final Project Report}\\[0.5cm] % Minor heading such as course title
	
	%------------------------------------------------
	%	Title
	%------------------------------------------------
	
	\HRule\\[0.4cm]
	
	{\huge\bfseries BLE Node Based Project}\\[0.4cm] % Title of your document
	
	\HRule\\[1.5cm]
	
	%------------------------------------------------
	%	Author(s)
	%------------------------------------------------
	
	\begin{minipage}[t]{0.4\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
			Dakota \textsc{Fisher}\\ % Your name
			200 344 336\newline \newline
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}[t]{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Professor}\\
			Karim \textsc{Naqvi} % Supervisor's name
		\end{flushright}
	\end{minipage}
	
	% If you don't want a supervisor, uncomment the two lines below and comment the code above
	%{\large\textit{Author}}\\
	%John \textsc{Smith} % Your name
	
	

%------------------------------------------------
	%	Logo
	%------------------------------------------------
		\vfill\vfill\vfill\vfill\vfill % Position the date 3/4 down the remaining page
	\includegraphics[width=.5\textwidth]{UR_Logo_Primary_Full_Colour_RGB.jpg} % Include a department/university logo - this will require the graphicx package
	
	%------------------------------------------------
	%	Date
	%------------------------------------------------
	

	
	{Last Modified\\\large\today} % Date, change the \today to a set date if you want to be precise		

	
	 % Push the date up 1/4 of the remaining page
	 
	%----------------------------------------------------------------------------------------	
\end{titlepage}

%----------------------------------------------------------------------------------------
% clear pagenumber on revision history page
\thispagestyle{empty}

%----------------------------------------------------------------------------------------
% Revision History
%----------------------------------------------------------------------------------------
\section*{Revision History}
\begin{tabularx}{\textwidth}{|Y|Y|Y|}
\hline
  \textbf{Revision Version} & \textbf{Revision Author} & \textbf{Revision Date}\\
\hline
1.0 & Dakota Fisher & April 11, 2019 \\
\hline
\end{tabularx}

\newpage


%----------------------------------------------------------------------------------------
%	Table of Contents
%----------------------------------------------------------------------------------------

\pagestyle{plain} %get rid of header/footer for toc page
\pagenumbering{roman}

\tableofcontents %put Table of Contents in

\listoffigures %put List of Figures in
\cleardoublepage %start new page

\pagestyle{plain} % put headers/footers back on
\pagenumbering{arabic}

%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------
%	Body of Document
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
\doublespacing % set document to be default double spaced


%----------------------------------------------------------------------------------------
%	Introduction
%----------------------------------------------------------------------------------------

\section{Introduction}
\paragraph{}
	This document outlines the project design, testing and usage overview for an embedded Bluetooth Low Energy (BLE) connected edge-central node system. The primary design goal is to have the edge node receive, and transmit raw, or slightly formatted data to the central node, where major computations and visualisation will be processed.  In a practical sense, the payload being transmitted between the central and edge nodes can be any packet format, customized for the application's purpose. The payload for this project is the accelerometer data read from the inertial measurement unit. 

\section{Project Functionality}
\paragraph{}
	The following is a reiteration of the project proposal to explain the overarching concept before delving into the design. At a high level, the desired results for the project are as follows. The project will provide a method to record readings of orientation from an Inertial Measurement Unit (IMU) attached to the STM32F100RB Microcontroller (STM). Once the STM has the IMU readings gathered, it will transmit them via the BlueFruit BLE breakout (BLE Module) to a central node over a BLE connection. The central node, which should be a Linux machine for the purposes of this project. The machine will interpret the input readings, and map the results to a cursor animated in a graphical program.
\newpage
\section{Requirements Breakdown}
\subsection{Proposal: Graphical Cursor Simulator (in a graphics engine)}
\subsubsection{Core Requirements}
\paragraph{}
the terminology \textbf{shall} is used to refer to things that must be completed, and \textbf{should} is used to depict requirements that are a good idea to implement. In order to provide the cursor capabilities outlined in the functionality above, the following must be completed:
\singlespacing % single spaced as lists space too much
\begin{enumerate}
\item The Edge node \textbf{shall} read the xyz-axis values from the Accelerometer, and Magnetometer of the IMU.
\item The Edge node \textbf{shall} send formatted packets of the IMU Data to the Central Node.
\item The Central node \textbf{shall} receive formatted packets from the Edge node.
\item The Central node \textbf{shall} interpret the formatted packets from the Edge node.
\item The Central node \textbf{shall} display a cursor based off the data from the Edge node
\end{enumerate}
\subsubsection{Extended Requirements}
\paragraph{}
	This section defines clarifications and suggested courses of action for implementation of the core requirements, while being optional as they aren't pivotal for the success of the project.
\begin{enumerate}
\item the Edge node \textbf{should} read the xyz-axis values from the Gyroscope of the IMU.
\begin{enumerate}
\item This is to enhance the orientation readings of the device.
\item The data should be inserted into the formatted packets outlined above.
\end{enumerate}
\end{enumerate}
\subsubsection{Extra Feature Requirements}
\paragraph{} 
	These features are optional, but the terminology \textbf{shall} and \textbf{should} are used to depict requirements for getting each proposed feature working.
\paragraph{Thermo-tracking heatmap Cursor:}
	This extra feature will take the temperature readings and use them to control the color and hue of the cursor, a simple visual update that appends to the scope of the project and works in parallel with the original cursor position project.
	\begin{enumerate}
\item The Edge node \textbf{shall} read the temperature data from the two IMU chips.
\item The Edge node \textbf{shall} transmit the temperature data in a formatted packet to the Central Node.
\item The Central node \textbf{shall} update the cursor colour based on the temperature data.
\end{enumerate}
	\newpage
	\doublespacing 
\section{Wiring Diagram}
\paragraph{} The wiring diagram for the system is depicted in Figure 1 below. The Edge node is comprised of a bluetooth module, and IMU attached to STM32 microcontroller. A battery bank is used to provide power to the edge node, and allow truly wireless conditions. When flashing or debugging the module, the power bank connection can be exchanged with the development machine. The USART serial port is still connected, for the sake of being able to debug when issues with the BLE device arise. VCC, VIN, V5v, and V5+ are used interchangeably to indicate a positive 5 volt value, as different documentation refers to different semantics, it is something to consider when following below.
	\begin{figure}[h!]
	\includegraphics[width=1\textwidth]{finalcircuit.png} 
	\caption{Central + Edge Node System Block Diagram}
	\end{figure}
	\paragraph{} The modules implemented are segmented on the STM32F100RB IC in Figure 1. The USART TX and RX pins correspond to PA2 and PA3 respectively on the STM. the SPI connections to communicate with the BLE SPI Friend are depicted in a direct relationship to the corresponding STM pins connected to the BLE device. The same schema is implemented for the Adafruit 9DOF Board, in which PB6 and PB7 are the SCL and SDA lines respectively for I2C communications. 
	\newpage
	\section{Design}
	\paragraph{} The edge system runs with a continuous main loop, which every cycle procedurally completes three to four tasks. First, it reads the accelerometer data. This process is relatively fast, and has little impact on the performance of the loop. Next, the system converts the accelerometer data to three strings, sending the data through the SPI connection each time a string is built. in a 'string - send - string - send - string - send' pattern. each send has two delay loops running, with a 240000 count per loop, which causes built in delays to pause the system by 20ms per transmission to allow processing time, and for data renewal. This means, that data is updated at a rate of 50 times per second. This is the life-cycle of the edge node, which still allows the UART data to be interpreted as well after all three axis data is sent to the Central node. The optional fourth task is to interpret and handle USART requests sent in via the serial connection.
	
	\paragraph{} The central node is broken into two separate python scripts. The first of which, is a bluetooth event handler. Which implements Adafruit's uart service example to handle the receiving of the bluetooth data. This runs off of the bluez service on the Ubuntu 16.04 LTS OS. It also acts as a client of a localhost tcp server, utilizing the multiprocessing package for Python. The decision to utilize this method over text document storage, is due to the simplicity of the messaging system it provides.
	
	\paragraph{} The second Python script on the central node expects data to come into it's server ports via the client script sending strings. The received strings are then parsed and stored as their relative x,y,z variables as integers. The pitch and roll angles are parsed from the data, and this is used to proportionally update the tkinter positions of the cursor. The values are done via a delta operation in which the delta is simply delta = (x += roll/10, y += pitch/10) 
	\newpage
	\section{Testing Procedure}
	\paragraph{} The goal of using TDD wasn't implemented due to time constraints, and the difficulty of building off of an existing code base that was not designed for TDD. Though, incremental validation testing was used as the project was built to ensure that desired results were being obtained at each phase of development. 
	\paragraph{} Following the data path, the first interaction was gathering accelerometer data from the IMU. The data was retrieved, and validated with breakpoints in the uVision debugger as well as running print statements over the already configured Serial connection. Once the data was validated to be reliable, and correctly formatted, development continued down the data path. There was no issues with the I2C transmission from the IMU, so there was no need for the USBee circuit analyser.
	\paragraph{} The STM received the data from the IMU, and then forwards a stringified version to the Adafruit BLE SPI Friend device in the form of an SDEP Packet. An SDEP Packet is a simple packet design which includes a 4 byte header consisting of 3 information bytes followed by a size byte, the rest of the packet is the payload data. To test this, the data sent in was recorded, and then the data received on the central node was validated against the expected results. There was no issues with the SPI transmission from the STM to the BLE Module, so there was no need for the USBee circuit analyser.
	
	\paragraph{} Finally, The data is transferred through a tcp peer connection to a secondary Python script running a tkinter window. The data is streamed in the terminal, as well as used to update the position of the cursor object. Thus, this portion of the project can be directly validated by physical interaction with the edge node and observing the results on the central node display.
	\section{Usage}
	\paragraph{} To use the system, power on the STM circuit by connecting the USB-B port on the STM to a power source, and wait a few seconds for the system to start up. Then, ensure that the bluetooth is enabled on the central node. Run the test.py python script and server.py files in two separate terminal windows, and then watch the test.py terminal until the data starts streaming in. Once this is the case, The cursor in the window should move related to the tilt of the device. The orientation of the device assumes that the positive x value is pointing to the user's right side, and the user is facing the screen displaying the sandbox.
\end{document}

%----------------------------------------------------------------------------------------
%	Document Style Templates
%----------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Section Nesting for Table of Contents
%
% A section or subsection can contain any number of nested children
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{First Title} %x
% \paragraph{} **~ Contents of First Title ~**
%
% \subsection{Second Title} %x.x
% \paragraph{} **~ Contents of Second Title ~**
%
% \subsubsection{Third Title} %x.x.x
% \paragraph{} **~ Contents of Third Title ~**
% 
% No more nesting allowed. capped at x.x.x sectioning
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------
%	Template References and Licenses
%----------------------------------------------------------------------------------------
% This paper integrates the following templates into a single document.
% There is no endorsement in any way shape or form from the template authors.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Academic Title Page
% LaTeX Template
% Version 2.0 (17/7/17)
%
% This template was downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% WikiBooks (LaTeX - Title Creation) with modifications by:
% Vel (vel@latextemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
% 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
