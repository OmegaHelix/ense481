from multiprocessing.connection import Listener
from graphics import *
import math



def roll(y,z):
    return math.atan2(float(y),float(z)) * (180.0/math.pi)

def pitch(x,y,z):
    return math.atan2(-float(x),math.hypot(float(y),float(z))) * (180.0/math.pi)

def main(address):
    x = 0
    y = 0
    z = 0
    newx = 0
    newy = 0
    newz = 0
    serv = Listener(address)
    win = GraphWin("SandBox", 1000, 500)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    c.undraw()
    c = Circle(Point(abs(100)%1000,abs(400)%500), 20)
    c.setFill("red")
    c.draw(win)
    while True:
        client= serv.accept()
        while True:
            msg = client.recv()
            print(msg)
            # ignore any readings without an# pattern
            if len(msg) > 2:
                # check if the data is just 'an-'
                if len(msg) is 3 and msg[2] is '-':
                    # add a '0' to make it a legal int
                    msg += "0"

                if msg[1] is 'x':
                    x = int(msg[2:])
                    print(newx)
                if msg[1] is 'y':
                    y = int(msg[2:])
                    print(newy)
                if msg[1] is 'z':
                    z = int(msg[2:])
                    print(z)
                rollVal = roll(y,z)
                pitchVal = pitch(x,y,z)
                print("x:", x)
                print("y:", y)
                print("z:", z)
                print("roll:",rollVal)
                print("pitch:",pitchVal)
		c.move(pitchVal * .1,rollVal * .1)
    win.close()    # Close window when done

main(('',5000))
