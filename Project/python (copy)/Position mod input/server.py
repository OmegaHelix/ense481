from multiprocessing.connection import Listener
from graphics import *

def main():
    win = GraphWin("My Circle", 100, 100)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    win.getMouse() # Pause to view result
    win.close()    # Close window when done

def child(conn):
    while True:
        msg = conn.recv()
        print(msg)
        conn.send(msg)


def mother(address):
    x = 0
    y = 0
    z = 0
    serv = Listener(address)
    win = GraphWin("SandBox", 1000, 500)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    c.undraw()
    c = Circle(Point(abs(100)%1000,abs(400)%500), 10)
    c.draw(win)
    while True:
        client= serv.accept()
        while True:
            msg = client.recv()
            print(msg)
            # ignore any readings without an# pattern
            if len(msg) > 2:
                # check if the data is just 'an-'
                if len(msg) is 3 and msg[2] is '-':
                    # add a '0' to make it a legal int
                    msg += "0"

                if msg[1] is 'x':

                    x = int(msg[2:])
                    print(x)
                if msg[1] is 'y':
                    y = int(msg[2:])
                    print(y)
                if msg[1] is 'z':
                    z = int(msg[2:])
                    print(z)
                c.undraw()
                c = Circle(Point(abs(x)%1000,abs(y)%500), 10)
                c.draw(win)
        #child(client)
    win.close()    # Close window when done

mother(('',5000))
