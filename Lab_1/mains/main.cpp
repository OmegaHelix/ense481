#include <iostream>
#include "../src/Accel.hpp"
#include "../src/Angles.hpp"
#include "../src/IMU.hpp"

using namespace std;

void Test(float ax, float ay, float az);

#define PI 3.14159265f

int main (void)
{
    IMU test;
    float g = test.getGravity();
    cout << g;
    Test(0,0,0);
    Test(0,0,17500);
        cout << "y: +45" << endl;
    Test(
            g * cos(45 * PI/180.f),
            0, 
            g * sin(45 * PI/180.f)
        );
    cout << "x: +45" << endl;
    Test(
            0,
            g * cos(45 * PI/180.f),
            g * sin(45 * PI/180.f)
        );
        cout << "worked 1 " << endl;
    Test(
        g*0.461105,
        g*0.082198,
        g*-0.887432);
    return 0;
}

void Test(float ax, float ay, float az)
{
    IMU imu(ax,ay,az);
    imu.calculateAll();
    cout << "float Input: " << ax << " " << ay << " " << az << endl;
    cout << "float Angle.pitch: " << imu.getAngles().pitch << endl;
    cout << "float Angle.roll: " << imu.getAngles().roll << endl;
    cout << "float Raw.ax: " << imu.getAccel_Raw().ax << endl;
    cout << "float Raw.ay: " << imu.getAccel_Raw().ay << endl;
    cout << "float Raw.az: " << imu.getAccel_Raw().az << endl;
    cout << "float Pilot.ax: " << imu.getAccel_Pilot().ax << endl;
    cout << "float Pilot.ay: " << imu.getAccel_Pilot().ay << endl;
    cout << "float Pilot.az: " << imu.getAccel_Pilot().az << endl << endl;
}
