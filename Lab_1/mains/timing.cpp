#include <iostream>
#include "../src/Accel.hpp"
#include "../src/Angles.hpp"
#include "../src/IMU.hpp"

#include <chrono>
#include <ctime>

int main()
{
    // Timing template taken from https://en.cppreference.com/w/cpp/chrono
    // Adjusted to test various timing cases.

/********/
    // Start Test Case 1
    // Describe Test
    std::cout << "T1: IMU Is Initialized with fake data,"
              << "initiating all conversions." << '\n';
    auto start = std::chrono::system_clock::now();
    // Start Computations
    IMU imu(1,2,3);
    // End Computations
    auto end = std::chrono::system_clock::now();
    // Calculate time consumed
    std::chrono::duration<double> elapsed_seconds = end-start;
    // Output Results
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n\n";
    // End Test Case 1

/********/
    // Start Test Case 2
    // Describe Test
    std::cout << "T2: IMU Is initialized and then given fake data," 
              << "initiating all conversions." << '\n';
    // Initializing IMU, since that's not part of the timing test
    IMU imu2;
    start = std::chrono::system_clock::now();
    // Start Computations
    imu2.setFakeAccel(1,2,3);
    // End computations
    end = std::chrono::system_clock::now();
    // Calculate time consumed
    elapsed_seconds = end-start;
    // Output Results
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n\n";
   // End Test Case 2

/********/
    // Start Test Case 3
    // Describe Test
    
    std::cout << "T3: Average Timing Case, Testing 0 to 20000 " 
              << "initiating all conversions." << '\n';
    // Initializing IMU, since that's not part of the timing test
    IMU imu3;
    start = std::chrono::system_clock::now();
    // Start Computations
    for(int i = 0; i < 20000; i++)
    imu3.setFakeAccel(i,i,i);
    // End computations
    end = std::chrono::system_clock::now();
    // Calculate time consumed
    elapsed_seconds = end-start;
    // Output Results
    std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
    std::cout << "Average time: " << elapsed_seconds.count()/20000.0 << "s\n\n";
   // End Test Case 3



/********/
    // Start Test Case 4
    // Describe Test
    
    std::cout << "T4: WCET Timing Case, Testing 0 to 20000\n10000 times in 0.1 increments. " 
              << "initiating all conversions." << '\n'
              << "Running loop 10 times instead of 10000\n";
    // Initializing IMU, since that's not part of the timing test
    IMU imu4;
    std::chrono::duration<double> wcet;
    for(int j = 0; j < 10 ; j++)
    {
        for(float i = 0; i < 20000; i += .1)
        {
            start = std::chrono::system_clock::now();
            // Start Computations
            imu4.setFakeAccel(i,i,i);
            // End computations
            end = std::chrono::system_clock::now();
            // Calculate time consumed
            elapsed_seconds = end-start;
            // Output Results
            if(elapsed_seconds > wcet) wcet = elapsed_seconds;
        // std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n";
        }
    }
          std::cerr << "wcet time: " << wcet.count() << "s\n\n";
   // End Test Case 4

    return 1;
}
