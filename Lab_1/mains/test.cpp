#include <iostream>
#include "../src/Accel.hpp"
#include "../src/Angles.hpp"
#include "../src/IMU.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"



// Angles Test cases

TEST_CASE ( "Angles is created properly with vector notation", "[Angles]")
{
    Angles angles = {1.f,2.f,3.f};
    REQUIRE(angles.roll == Approx(1.f));
    REQUIRE(angles.pitch == Approx(2.f));
    REQUIRE(angles.yaw == Approx(3.f));
}

TEST_CASE ( "Angles is created then filled with floats", "[Angles]")
{
    Angles angles;
    angles.roll = 1.f;
    angles.pitch = 2.f;
    angles.yaw = 3.f;
    REQUIRE(angles.roll == Approx(1.f));
    REQUIRE(angles.pitch == Approx(2.f));
    REQUIRE(angles.yaw == Approx(3.f));
}

// Accel Test cases

TEST_CASE ( "Accel is created properly with vector notation", "[Accel]")
{
    Accel accel = {1.f,2.f,3.f};
    REQUIRE(accel.ax == Approx(1.f));
    REQUIRE(accel.ay == Approx(2.f));
    REQUIRE(accel.az == Approx(3.f));
}

TEST_CASE ( "Accel is created then filled with floats", "[Accel]")
{
    Accel accel;
    accel.ax = 1.f;
    accel.ay = 2.f;
    accel.az = 3.f;
    REQUIRE(accel.ax == Approx(1.f));
    REQUIRE(accel.ay == Approx(2.f));
    REQUIRE(accel.az == Approx(3.f));
}
// IMU Test cases

TEST_CASE( "imu is created with zero values", "[IMU]") 
{
    IMU imu;
    REQUIRE(imu.getAccel_Raw().ax == Approx(0.f));
    REQUIRE(imu.getAccel_Raw().ay == Approx(0.f));
    REQUIRE(imu.getAccel_Raw().az == Approx(0.f));
}


TEST_CASE( "imu is created with 3 float values", "[IMU]") 
{
    IMU imu(1.f,2.f,3.f);
    REQUIRE(imu.getAccel_Raw().ax == Approx(1.f));
    REQUIRE(imu.getAccel_Raw().ay == Approx(2.f));
    REQUIRE(imu.getAccel_Raw().az == Approx(3.f));
}


TEST_CASE( "imu is created with Accel struct", "[IMU]") 
{
    Accel accel = {1.f,2.f,3.f};
    IMU imu(accel);
    REQUIRE(imu.getAccel_Raw().ax == Approx(1.f));
    REQUIRE(imu.getAccel_Raw().ay == Approx(2.f));
    REQUIRE(imu.getAccel_Raw().az == Approx(3.f));
}


TEST_CASE( "imu getAccel_Raw returns raw acceleration data", "[IMU]") 
{
    IMU imu(1.f,2.f,3.f);
    REQUIRE(imu.getAccel_Raw().ax == Approx(1.f));
    REQUIRE(imu.getAccel_Raw().ay == Approx(2.f));
    REQUIRE(imu.getAccel_Raw().az == Approx(3.f));
}

TEST_CASE( "imu getAccel_Pilot returns m/s^2 acceleration data", "[IMU]")
{
    IMU imu(1.f,2.f,3.f);
    float g= imu.getGravity();
    REQUIRE(imu.getAccel_Pilot().ax == Approx(1.f / g));
    REQUIRE(imu.getAccel_Pilot().ay == Approx(2.f / g));
    REQUIRE(imu.getAccel_Pilot().az == Approx(3.f / g));
}

TEST_CASE( "imu getAngles returns angle data", "[IMU]") 
{
    IMU imu;
    float g= imu.getGravity();
    INFO("The World coordinates stationary vector is {0,0,g}")
    imu.setFakeAccel(0.f,0.f,g);
    REQUIRE(imu.getAngles().roll == Approx(0));
    REQUIRE(imu.getAngles().pitch == Approx(0));
    REQUIRE(imu.getAngles().yaw == Approx(0));
}

TEST_CASE( "imu detects rotation in +45 degrees along the x-axis", "[IMU]")
{
    IMU imu;
    float g= imu.getGravity();
    INFO("The devices is rotated +45 degrees along the y-axis")
    imu.setFakeAccel( 0,g * cosf(45 * PI/180.f), g * sinf(45 * PI/180.f));
    REQUIRE(imu.getAngles().roll == Approx(0));
    REQUIRE(imu.getAngles().pitch == Approx(45));
    REQUIRE(imu.getAngles().yaw == Approx(0));
}


TEST_CASE( "imu detects rotation in +45 degrees along the y-axis", "[IMU]")
{
    IMU imu;
    float g= imu.getGravity();
    INFO("The devices is rotated +45 degrees along the y-axis")
    imu.setFakeAccel( g * cosf(45 * PI/180.f),0, g * sinf(45 * PI/180.f));
    REQUIRE(imu.getAngles().roll == Approx(-45));
    REQUIRE(imu.getAngles().pitch == Approx(0));
    REQUIRE(imu.getAngles().yaw == Approx(0));
}

TEST_CASE( "Worked Example 1", "[IMU]")
{
    IMU imu;
    float g= imu.getGravity();
    INFO("This is Worked Example 1")
    imu.setFakeAccel(g*0.461105,g*0.082198,g*-0.887432);
    REQUIRE(imu.getAngles().roll == Approx(-27.3561));
    REQUIRE(imu.getAngles().pitch == Approx(174.7081));
    REQUIRE(imu.getAngles().yaw == Approx(0));
}


TEST_CASE("imu calculateNorm returns 1 when no linear acceleration is present"
            , "[IMU]") 
{
    IMU imu;
    float g= imu.getGravity();
    INFO("working with the base unit g, the result should be 1")
    imu.setFakeAccel(0.f,0.f,g);
    REQUIRE(imu.getNorm() == Approx(1));
}
