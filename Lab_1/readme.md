# Lab1
## Understanding the 3-Axis Accelerometer

## Make instructions

1. `make all` makes the test, timing, and main executables.
2. `make test` makes the test executable
3. `make timing` makes the timing executable
4. `make main` makes the main executable
5. `make fullclean` removes all unrequired files from the directories
6. `make srcclean` removes all non-essential files from the `./src` directory
7. `make outclean` removes all non-essential files from the `./exe` directory
8. `make objclean` removes all non-essential files from the `./build` directory

## Running Instructions.
After running a make command above (1,2,3,4) the executable can be found in the `./exe/` directory.
1. `./exe/test` runs the test cases
2. `./exe/timing` runs the timing tests
3. `./exe/main` runs the main program


### Completed Features:

1. The IMU Class is a data container and status class to manage and manipulate the data from an accelerometer.
2. The function `IMU::getAccel_Pilot()` returns the value of the accelerometer in g's. Can be multiplied by 9.8m/s^2 to get the accelerometer vector in m/s^2.
3. When the IMU is given a vector of 3 floats, or 3 individual floats corresponding to the x, y, and z raw accelerometer values, it automatically computes the relative to g accelerometer values, the Norm, and Roll and Pitch values.
4. The Test cases for the software are located in `test/testing.cpp` and assure that the function returns expected values for select test cases.
   1. The next steps would be to change to a test loop for each function.
5. The `test/timing.cpp` for this program to take in data, and propogate all the calculated variables is: 
   >>T1: IMU Is Initialized with fake data,initiating all conversions.\
       elapsed time: 0.0004601s

    >>T2: IMU Is initialized and then given fake data,initiating all    conversions.\
        elapsed time: 2.5e-06s

    >>T3: Average Timing Case, Testing 0 to 20000 initiating all conversions.\
    elapsed time: 0.0022284s\
    Average time: 1.1142e-07s

    >>T4: WCET Timing Case, Ran over the values of 0 to 20000 in 0.1 increments. Looped through these tests 10000 times to experimentally discover the worst case experienced execution time. Testing 0 to 20000 initiating all conversions.\
    wcet time: 0.126339s
6. The `bool IMU::Linear_Acceleration` is set based on whether the device is under linear acceleration or not.
7. Every function returns true, or the return value upon completion.

### Requirements:
1. Write a function getAccel which obtains the three acceleration components. These should be floats,
with units of m/s^2. At this time this will be “fake”, and the three components will be provided by some other stub code, or from a data file. The fake could include translation from raw readings to m/s^2.

2. Write a function that reads an acceleration value (a float 3-tuple), and figures out some things:
   1.  Figure out the direction of the gravity vector in “pilot” coordinates. If the IMU were perfectly aligned with world coordinates, this value should be (0.0f, 0.0f, 9.8f), where g = 9.8m/s^2. If the IMU were then rotated +45◦ about the xw axis, it should measure 
   (g cos 45◦), 0, g sin(45◦)). If the IMU, starting from the home position, were instead rotated by +45◦ about the yw axis, then
   it should measure (0, g cos(45◦), g sin(45◦)).
   2. Figure out, assuming the IMU is not accelerating, the pitch (θp)  and roll (θr) angles
   3. If the IMU is accelerating, say because a human is moving it with acceleration ar, the total acceleration measured will be the vector sum g + ar. Figure out if it’s possible to untangle this: to extract the roll and pitch angle, and the externally applied acceleration, and the acceleration components due simply to gravity.
3. I’ve provided a file of IMU acceleration data, with 293 records, at a sampling rate of 10Hz. While recording the data, I moved the IMU and took a video of the motion. In this video the xw axis is to my right, the yw axis is coming out of the screen, and the zw axis is up. You can see that the motion takes place over several phases. 
   1. IMU is stationary, in the home position, with the XY Z axes aligned with the world axes (x_w,y_w,z_w).
   2. The IMU is moved in the +z direction about 40 cm.
   3. The IMU is rotated CCW about the x-axis to −90◦.
   4. The IMU is rotated CW about the x-axis to +90◦.
   5. The IMU is rotated CCW about the x-axis back to 0◦.
   6. The IMU is rotated CCW about the y-axis to −90◦.
   7. The IMU is rotated CW about the y-axis to +90◦.
   8. The IMU is rotated CCW about the y-axis back to 0◦. (We are now back in the horizontal position, but at about 40cm elevation)
   9. The IMU is moved in −z direction back down to the home position. Of course the motions are not calibrated, but they give an idea of what’s happening in the real world.

    Can you extract readings at various points in this sequence, and do your calculations agree with what’s
    happening in the video? The acceleration readings in the data file are stored as int16_t values. This is the format which is produced by the 9DOF module. You should convert them to m/s2 to work with your existing code.
4. Write unit tests for your code. You can use lightweight C++ test frameworks like doctest1, catch2, or cxxtest3.
5. Time your code. What is the average execution time for your function of step 2? Can you estimate the worst case execution time (WCET)?
6. The code should recognize when the object is undergoing some acceleration and somehow communicate this.
7. The code should also indicate whether it succeeded or failed.