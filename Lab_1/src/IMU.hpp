#pragma once
#include "Accel.hpp"
#include "Angles.hpp"
#include <cmath>

#define PI 3.14159265f
/**
 * @class IMU
 * @brief Inertial Measurement Unit
 * @details This class holds an accelerometer reading.
 * It manipulates and converts the values to
 * useable formats and abstractions.
 * @author Dakota Fisher
 * @date 2019
 * @version 1.0
 */
class IMU
{
    private:
        /**
         * @brief Accelerometer_Raw holds the readings from the device
         */
        Accel Accelerometer_Raw = {0,0,0};
        /**
         * @brief Accelerometer_Pilot holds the readings in m/s^2
         */
        Accel Accelerometer_Pilot = {0,0,0};
        /**
         * @brief Angles holds the roll, pitch and yaw angles in degrees.
         */
        Angles Angle = {0,0,0};
        /**
         * @brief GRAVITY holds the gravity value for Raw data.
         */
        float GRAVITY = 17500; //magic number. replace with calib func.
        /**
         * @brief Norm holds the norm length of the acceleration vectors
         */
        float Norm = 0;
        bool Linear_Acceleration = false;
    public:

        IMU();

        IMU(float ax, float ay, float az);

        IMU(Accel accelData);

        Accel getAccel_Raw();
        Accel getAccel_Pilot();
        Angles getAngles();
        float getGravity();
        float getNorm();
        bool getLinear_Acceleration();

        bool calculateAllAngles();
        bool calculatePitchAngle();
        bool calculateRollAngle();
        bool calculateAccelerometer_Pilot();
        bool calculateNorm();
        bool calculateAll();

        bool setFakeAccel(float ax, float ay, float az);
};
