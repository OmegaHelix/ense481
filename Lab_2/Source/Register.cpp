#include "Register.h"

Register::Register(uint32_t Reg) :
  reg(*reinterpret_cast<uint32_t volatile*>(Reg))
{}

void Register::Or(uint32_t Value)
{
  reg |= Value;
}

void Register::And(uint32_t Value)
{
  reg &= Value;
}

uint32_t Register::Read(void)
{
  return reg;
}

void Register::Write(uint32_t Write)
{
  reg = Write;
}

void Register::Set(enum Register::bit bit)
{
        reg |= (0x1 << bit);
}

void Register::Reset(enum Register::bit bit)
{
        reg &= ~(0x1 << bit);
}

Register::~Register()
{
}
