#ifndef __TIMER
#define __TIMER

/******************************************************************************
* File: TIMER.h
*
* Description: This file holds the basic TIMER Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			TIMER(uint32_t):
*				This function Creates an instance of an TIMER to manipulate.
*
*
*			~TIMER();
*			void Or(enum TIMER::TIMER_, uint32_t);
*			void And(enum TIMER::TIMER_, uint32_t);
*			void Set(enum TIMER::TIMER_, uint32_t);
*			uint32_t Read(enum TIMER::TIMER_);
*			enum TIMER_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_TIMER.Or(TIMER::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class TIMER
{
  private:
    // These registers can't be static because they're created as an entity of TIMER Drivers
    // Any instance or TIMER::func()
    //call will reference these registers for their TIMER#.
      Register _CR1;
      Register _CR2;
      Register _SMCR;
      Register _DIER;
      Register _SR;
      Register _EGR;
      Register _CCMR1;
      Register _CCMR2;
      Register _CCER;
      Register _CNT;
      Register _PSC;
      Register _ARR;
      Register _CCR1;
      Register _CCR2;
      Register _CCR3;
      Register _CCR4;
      Register _DCR;
      Register _DMAR;
  public:
                enum TIMER_ {CR1,CR2,SMCR,DIER,SR,EGR
                            ,CCMR1,CCMR2,CCER,CNT,PSC,ARR
                            ,CCR1,CCR2,CCR3,CCR4,DCR,DMAR};
                TIMER(uint32_t);
                ~TIMER();
                void Or(enum TIMER::TIMER_, uint32_t);
                void And(enum TIMER::TIMER_, uint32_t);
                void Write(enum TIMER::TIMER_, uint32_t);
                void Set(enum TIMER::TIMER_, Register::bit);
                void Reset(enum TIMER::TIMER_, Register::bit);
                uint32_t Read(enum TIMER::TIMER_);

};

#endif



