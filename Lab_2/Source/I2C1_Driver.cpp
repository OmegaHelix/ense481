#include "I2C1_Driver.h"
#include "GPIO.h"
#include <arm_compat.h>
// Base address for the I2C1 Registers
const uint32_t I2C1_Driver::_I2C1 = 0x40005400;
bool I2C1_Driver::isopen = false;

//I2C1 Registers
I2C I2C1_Driver::I2C1(_I2C1);

I2C1_Driver::I2C1_Driver(){}
I2C1_Driver::~I2C1_Driver(){}


uint32_t I2C1_Driver::Write(uint8_t* buffer, uint32_t numBytes, uint8_t slaveAddress)
{
  uint32_t timeout = 0;
  // if there's bytes to send
  if(numBytes)
  {
    // wait for the line to stop being busy
    while((I2C1.Read(I2C::SR2) & 0x2) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    // start enable
    start();


    // Check event MASTER_MODE_SELECT
    // while(0x00030001)
    uint32_t flags1;
    uint32_t flags2;
    bool flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1) & 0x00FF;
      flags2 =	(I2C1.Read(I2C::SR2) & 0x00FF) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00030001)
                == 0x00030001;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    // send Slave address | Direction_Transmitter
    Send7BitAddress(slaveAddress, false);
    //Check Event MASTER_TRANSMITTER_MODE_SELECTED
    flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1);
      flags2 =	(I2C1.Read(I2C::SR2)) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00070082)
                == 0x00070082;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
    //Send first Byte
    SendData(*buffer++);

    while(--numBytes)
    {
      // Wait on BTF (GetFlagStatus)
          // wait for the line to Byte Transfer Finish
    while((!(I2C1.Read(I2C::SR1) & 0x4)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
      //Send Byte
        SendData(*buffer++);
    }

    //Wait on BTF (GetFlagStatus)
    // wait for the line to Byte Transfer Finish
    while((!(I2C1.Read(I2C::SR1) & 0x4)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
    // STOP
    stop();
    //Wait on STOPF (GetFlagStatus)
    // wait for the line to Stop flag to be set
    while(((I2C1.Read(I2C::SR1) & 0x010)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
  }
  // return success
  return 0x1;
}

bool I2C1_Driver::SendData(uint8_t data)
{
  I2C1.Write(I2C::DR, static_cast<uint32_t>(data));
  return true;
}

bool I2C1_Driver::Send7BitAddress(uint8_t slaveAddress, bool RtrueWfalse)
{
  if(RtrueWfalse)
  {
    slaveAddress |= 0x01;
  }
  else
  {
    slaveAddress &= ~0x01;
  }

  I2C1.Write(I2C::DR, static_cast<uint32_t>(slaveAddress) & 0x0000FFFF);

  return true;
}



uint32_t I2C1_Driver::Read( uint8_t * buffer, uint32_t numBytes, uint8_t slaveAddress)
{
  uint32_t timeout = 0;

  if(numBytes == 0)
  {
    // return, the transaction is complete
    return 0x1;
  }
    // wait for the line to stop being busy
    while((I2C1.Read(I2C::SR2) & 0x2) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    // tell what register to read
    // start enable
    start();


    // Check event MASTER_MODE_SELECT
    // while(0x00030001)
    uint32_t flags1;
    uint32_t flags2;
    bool flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1) & 0x00FF;
      flags2 =	(I2C1.Read(I2C::SR2) & 0x00FF) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00030001)
                == 0x00030001;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    // send Slave address | Direction_Transmitter
    Send7BitAddress(slaveAddress, false);
    //Check Event MASTER_TRANSMITTER_MODE_SELECTED
    flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1);
      flags2 =	(I2C1.Read(I2C::SR2)) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00070082)
                == 0x00070082;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
    //Send first Byte
    SendData(*buffer);



    // ACK
    I2C1.Set(I2C::CR1, Register::b10);
    // NACK
    I2C1.Reset(I2C::CR1, Register::b11);
    //I2C1.And(I2C::CR1, 0xF7FF);

    // start enable
    start();


    // Check event MASTER_MODE_SELECT
    // while(0x00030001)
     flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1) & 0x00FF;
      flags2 =	(I2C1.Read(I2C::SR2) & 0x00FF) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00030001)
                == 0x00030001;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    // send Slave address | Direction_Reciever
    Send7BitAddress(slaveAddress, true);


    // I2C_FLAG_ADDR
    while((!(I2C1.Read(I2C::SR1) & 0x10000002)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

    if (numBytes == 1)
    { /* read 1 byte */

      // clear ACK
      I2C1.Reset(I2C::CR1, Register::b10);

      __disable_irq();
      I2C1.Read(I2C::SR2);
      stop();
      __enable_irq();


      // FLAG_RXNE
    while((!(I2C1.Read(I2C::SR1) & 0x10000040)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

      *buffer++ = RecieveData();
    }
    else if (numBytes == 2)
    { /* read 2 bytes */

     //I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Next);

      __disable_irq();
      // clear ADDR flag
      I2C1.Read(I2C::SR2);
      // clear ACK bit
      I2C1.Reset(I2C::CR1, Register::b10);
      __enable_irq();


    // wait for the line to Byte Transfer Finish
    while((I2C1.Read(I2C::SR2) & 0x10000004) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }

      //Stop and Read twice
    __disable_irq();
    stop();
    *buffer++ = I2C1.Read(I2C::DR);
    __enable_irq();

    *buffer++ = I2C1.Read(I2C::DR);
    }
    else
    { /* read 3 or more bytes */

      // clear ADDR flag
      I2C1.Read(I2C::SR2);
      // all bytes over 3
      while(numBytes-- != 3)
      {
        // wait for the line to Byte Transfer Finish
        while((I2C1.Read(I2C::SR2) & 0x10000004) && (timeout++ < 0xFFFF))
        { // assembly to avoid optimizing out loop
          asm("");
        }
        // if the wait timed out
        if(timeout >= 0xFFFF)
        {
          // return error
          return 0x0;
        }
        *buffer++ = I2C1.Read(I2C::DR);
      }

      /**
      * N-2 Byte
      */

      // wait for the line to Byte Transfer Finish
      while((I2C1.Read(I2C::SR2) & 0x10000004) && (timeout++ < 0xFFFF))
      { // assembly to avoid optimizing out loop
        asm("");
      }
      // if the wait timed out
      if(timeout >= 0xFFFF)
      {
        // return error
        return 0x0;
      }
      // clear ACK bit
      I2C1.Reset(I2C::CR1, Register::b10);

    //N-2
    __disable_irq();
    *buffer++ = I2C1.Read(I2C::DR);
    stop();
    __enable_irq();

    //N-1
    *buffer++ = I2C1.Read(I2C::DR);


    // Check event MASTER_BYTE_RECIEVED
    // while(0x00030001)
    uint32_t flags1;
    uint32_t flags2;
    bool flags = false;
    timeout = 0;
    while(!flags && (timeout++ < 0xFFFF))
    {
      flags1 =	I2C1.Read(I2C::SR1) & 0x00FF;
      flags2 =	(I2C1.Read(I2C::SR2) & 0x00FF) << 16;
      flags  = (((flags1 | flags2)
                & 0x00FFFFFF)
                & 0x00030040)
                == 0x00030040;
    }
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
    // N
    *buffer++ = I2C1.Read(I2C::DR);








    }
    //Wait on STOPF (GetFlagStatus)
    // wait for the line to Stop flag to be set
    while(((I2C1.Read(I2C::SR1) & 0x010)) && (timeout++ < 0xFFFF))
    { // assembly to avoid optimizing out loop
      asm("");
    }
    // if the wait timed out
    if(timeout >= 0xFFFF)
    {
      // return error
      return 0x0;
    }
  return 0x1;
}

void I2C1_Driver::open()
{
  // only reconfigure if the driver isn't in use
  if(isopen)
  {
    return;
  }
  //Enable GPIOB Clock
  RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPBEN);
    //enable AF Clock
  RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_AFIOEN);
    //Enable I2C Clock
    RCC2::Or(RCC2::APB1ENR, RCC2::_APB1ENR_I2C1EN);
  //Configure SCL and SDA pins
  //SCL Pin
    // AF Output Open Drain (11 CNF 11 Mode = 0xF)
  //Set GPIOA pin 6 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRL, 0xF << 24);
  //SDA Pin
    // AF Output Open Drain (11 CNF 11 Mode = 0xF)
  //Set GPIOA pin 7 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRL, 0xF << 28);

    /**
    * CCR Configuration
    */
    //Set the clock frequency to 24Mhz
    I2C1.Or(I2C::CR2, 24u);
    //Disable I2C1 to configure TRISE
    I2C1.And(I2C::CR1, 0xFFFE);
    //24Mhz/600000 = 40 = 0x28
    // 2 Byte Register, Mask to clear upper bits.
    uint32_t CCRTemp = I2C1.Read(I2C::CCR) & 0x0000FFFFu;
    CCRTemp |= 0x0028 | 0x8000;
    // Configure TRISE
    I2C1.Write(I2C::TRISE, 0x8);
    // Write CCR Config (Fast Mode + clock)
    I2C1.Write(I2C::CCR, CCRTemp);
    // Enable SPE bit
    I2C1.Or(I2C::CR1, 0x1);

    /**
    * CR1 Configuration
    */
    // Ack_Enable = 0x0400
    // CR1 clear mask = 0xFBF5
    uint32_t CR1Temp = I2C1.Read(I2C::CR1);
    CR1Temp &= 0xFBF5;
    CR1Temp |= 0x400;
    I2C1.Write(I2C::CR1, CR1Temp);
    // 7Bit address = 0x4000
    I2C1.Write(I2C::OAR1, 0x4000);
    // Enable SPE bit
    I2C1.Or(I2C::CR1, 0x1);
    isopen = true;
}

bool I2C1_Driver::GetStatus( uint32_t slave7BitAddress )
{
  uint32_t timeout = 100000;
  //clear flag AF
  //enable acknowledge
  //generate start
  //while waiting for start
  //send 7 bit address
  //while !selected && timeout--
  //get flag, return error or true
  return true;
}


uint8_t I2C1_Driver::RecieveData()
{
  return static_cast<uint8_t>(I2C1.Read(I2C::DR) & 0x000000FF);
}

void I2C1_Driver::close()
{
}

void I2C1_Driver::start()
{
  I2C1.Set(I2C::CR1, Register::b8); // set the start enable bit
}

void I2C1_Driver::stop()
{
  I2C1.Set(I2C::CR1, Register::b9); //set the stop enable bit.
}

void I2C1_Driver::startAccel()
{
  // Configure CTRL_REG1_A Register
  uint8_t reg = 0x20;
  uint8_t value = 0x27;
  uint8_t buffer[2] = {reg, value};
  Write(buffer, 2, 0x32);

  // Configure CTRL_REG4_A Register
  reg = 0x23;
  value = 0x00;
  buffer[0] =  reg;
  buffer[1] = value;
  Write(buffer, 2, 0x32);
}
void I2C1_Driver::startMag()
{
  // Configure CTRL_REG_M Register
  uint8_t reg = 0x00;
  uint8_t value = 0x9C;
  uint8_t buffer[2] = {reg, value};
  Write(buffer, 2, 0x3C);

  // Configure MR_REG_M Register
  reg = 0x02;
  value = 0x00;
  buffer[0] =  reg;
  buffer[1] = value;
  Write(buffer, 2, 0x3C);
}




Accel I2C1_Driver::readAccel()
{
  //Read( uint8_t * buffer, uint32_t numBytes, uint8_t slaveAddress)
  uint8_t axLBuffer[1] = {0x28};
  uint8_t axHBuffer[1] = {0x29};
  uint8_t ayLBuffer[1] = {0x2A};
  uint8_t ayHBuffer[1] = {0x2B};
  uint8_t azLBuffer[1] = {0x2C};
  uint8_t azHBuffer[1] = {0x2D};
  uint8_t slaveAddress = 0x32;
  // Write register address into Read
  // Read register value into same buffer
  // input: register to read
  // output: register value
  Read(axHBuffer,1, slaveAddress);
  Read(axLBuffer,1, slaveAddress);
  Read(ayHBuffer,1, slaveAddress);
  Read(ayLBuffer,1, slaveAddress);
  Read(azHBuffer,1, slaveAddress);
  Read(azLBuffer,1, slaveAddress);
  Accel test =
     {static_cast<float>(static_cast<int16_t>((*axHBuffer << 8 | *axLBuffer))),
      static_cast<float>(static_cast<int16_t>((*ayHBuffer << 8 | *ayLBuffer))),
      static_cast<float>(static_cast<int16_t>((*azHBuffer << 8 | *azLBuffer)))};
  // correct acceleration on X axis to bounds of +-2g
  // if(test.ax < -17000) test.ax /=2;
  return test;
}




Accel I2C1_Driver::readMag()
{
  //Read( uint8_t * buffer, uint32_t numBytes, uint8_t slaveAddress)
  uint8_t addrUpdate[1] = {0x02};
  uint8_t mxLBuffer[1] = {0x28};
  uint8_t mxHBuffer[1] = {0x29};
  uint8_t myLBuffer[1] = {0x2A};
  uint8_t myHBuffer[1] = {0x2B};
  uint8_t mzLBuffer[1] = {0x2C};
  uint8_t mzHBuffer[1] = {0x2D};
  uint8_t slaveAddress = 0x3C;
  // Write register address into Read
  // Read register value into same buffer
  // input: register to read
  // output: register value
  Read(addrUpdate,1, slaveAddress); // reading this updates the addr
  Read(mxHBuffer,1, slaveAddress);
  Read(mxLBuffer,1, slaveAddress);
  Read(myHBuffer,1, slaveAddress);
  Read(myLBuffer,1, slaveAddress);
  Read(mzHBuffer,1, slaveAddress);
  Read(mzLBuffer,1, slaveAddress);
  Accel test =
     {static_cast<float>(static_cast<int16_t>((*mxLBuffer << 8 | *mxHBuffer))),
      static_cast<float>(static_cast<int16_t>((*myLBuffer << 8 | *myHBuffer))),
      static_cast<float>(static_cast<int16_t>((*mzLBuffer << 8 | *mzHBuffer)))};
  // correct acceleration on X axis to bounds of +-2g
   if(test.ax < -17000) test.ax /=2;
  return test;
}

uint32_t I2C1_Driver::readTemp()
{
    uint8_t slaveAddress = 0x3C;
    uint8_t tmpLBuffer[1] = {0x32};
    uint8_t tmpHBuffer[1] = {0x31};
    Read(tmpHBuffer,1, slaveAddress);
    Read(tmpLBuffer,1, slaveAddress);
    uint16_t temp = static_cast<uint16_t>(tmpHBuffer[0]) << 8 | (static_cast<uint16_t>(tmpLBuffer[0]) & 0x00FF);
    return (static_cast<int16_t>(temp) >> 4);
}
