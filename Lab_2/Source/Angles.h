#pragma once
/**
 * @brief       Angles 3-tuple
 * @details     This struct holds 3 floats to represent three Angles
 *              of acceleration. Their values are in degrees.
 * @author      Dakota Fisher
 * @date        2019
 * @version     1.0
 */
struct Angles
{
    float roll; // roll is the roll angle of the accelerometer
    float pitch; // pitch is the pitch angle of the accelerometer
    float yaw; // yaw is the yaw angle of the accelerometer
};
