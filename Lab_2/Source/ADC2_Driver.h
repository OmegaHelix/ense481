#ifndef __ADC1_Driver
#define __ADC1_Driver

#include "ADC.h"
#include "GPIO.h"
#include "RCC.h"

class ADC1_Driver
{
  private:
    static ADC ADC1;
    static const uint32_t ADC1_BASE;
    static uint16_t data;
    static bool ADC1_data_ready;

  public:
    ADC1_Driver();
    ~ADC1_Driver();
    static void open();
    static void close();
    static void start();
    static void stop();
    static float getVoltage();
    static void ADC1_2_IRQHandler();

  friend class CLI_Driver;
};


extern "C" void ADC1_2_IRQHandler(void);

#endif

