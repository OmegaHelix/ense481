#ifndef __GPIO_PORT
#define __GPIO_PORT

/******************************************************************************
* File: GPIO_PORT.h
*
* Description: This file holds the basic GPIO_PORT Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			GPIO_PORT(uint32_t):
*				This function Creates an instance of an GPIO_PORT to manipulate.
*
*
*			~GPIO_PORT();
*			void Or(enum GPIO_PORT::GPIO_PORT_, uint32_t);
*			void And(enum GPIO_PORT::GPIO_PORT_, uint32_t);
*			void Set(enum GPIO_PORT::GPIO_PORT_, uint32_t);
*			void Reset(enum GPIO_PORT::GPIO_PORT_, uint32_t);
*			void Write(enum GPIO_PORT::GPIO_PORT_, uint32_t);
*			uint32_t Read(enum GPIO_PORT::GPIO_PORT_);
*			enum GPIO_PORT_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_GPIO_PORT.Or(GPIO_PORT::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class GPIO_PORT
{
  private:
    // These registers can't be static because they're created as an entity of GPIO
    // Any instance or GPIO_PORT::func() call will reference these same Registers.
    Register _CRL;
    Register _CRH;
    Register _IDR;
    Register _ODR;
    Register _BSRR;
    Register _BRR;
    Register _LCKR;

  public:
                enum PORT_ {CRL,CRH,IDR,ODR,BSRR,BRR, LCKR};
                GPIO_PORT(uint32_t);
                ~GPIO_PORT();
                void Or(enum GPIO_PORT::PORT_, uint32_t);
                void And(enum GPIO_PORT::PORT_, uint32_t);
    void Write(enum GPIO_PORT::PORT_, uint32_t);
                void Set(enum GPIO_PORT::PORT_, Register::bit);
                void Reset(enum GPIO_PORT::PORT_, Register::bit);
                uint32_t Read(enum GPIO_PORT::PORT_);

};

#endif



