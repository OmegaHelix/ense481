#ifndef __I2C1_Driver
#define __I2C1_Driver

#include "I2C.h"
#include "GPIO.h"
#include "RCC.h"
#include "IMU.h"


// This driver is just for I2C 2
// Entire class is static to allow easier usage with C IRQ's
class I2C1_Driver
{
  private:
    // I2C1 register for the driver to utilize
    static I2C I2C1;

  public:

    // constant for I2C1
    static const uint32_t _I2C1;
    static bool isopen;

    //Constructor and Destructor
    I2C1_Driver();
    ~I2C1_Driver();

    //Driver functions
    static void open();
    static void close();
    static void start();
    static void stop();
    static bool GetStatus(uint32_t);

    //Usage Functions
    static uint32_t Write(uint8_t*, uint32_t, uint8_t);
    static uint32_t Read(uint8_t*, uint32_t, uint8_t);
    static bool SendData(uint8_t);
    static bool Send7BitAddress(uint8_t, bool);
    static uint8_t RecieveData();

    static void startAccel();
    static void startMag();
    static void startTemp();

    static Accel readAccel();
    static Accel readMag();
    static uint32_t readTemp();

    // static class interrupt function to be called from C IRQ handler
        static void I2C1_IRQHandler(void);

  // befriend class CLI_Driver to let it have complete inheritance
  friend class CLI_Driver;
};

//interrupt data

extern "C" void I2C1_IRQHandler(void);


#endif



