#include "CLI_Driver.h"

  static uint8_t HELP[]              = "HELP";
  static uint8_t HELP_CASE[]        = "Commands are case insensitive";
  static uint8_t HELP_LINE[]         = "The possible commands are:";
  static uint8_t HELP_DESC[]         = "\tHELP provides the above list of command descriptions";
  static uint8_t LED_LINE[]          = "LED <# 1 through 8 or ALL> <ON, OFF, or STATE>";
  static uint8_t LED_ON_DESC_1[]     = "\tLED # ON\t provides power to the LED";
  static uint8_t LED_ON_DESC_2[]     = "\tLED ALL ON\t provides power to all LED's";
  static uint8_t LED_OFF_DESC_1[]    = "\tLED # OFF\t turns off power to the LED";
  static uint8_t LED_OFF_DESC_2[]    = "\tLED ALL OFF\t turns off power to all LED's";
  static uint8_t LED_STATE_DESC_1[]  = "\tLED # STATE\t provides power status of the LED";
  static uint8_t LED_STATE_DESC_2[]  = "\tLED ALL STATE\t provides power status of all LED's";
  static uint8_t SERVO_DESC_1[]  			 = "SERVO < degrees or percent value>";
  static uint8_t SERVO_DESC_2[]  			 = "\tDefault unit is degrees, use a % sign to use percentage unit";
  static uint8_t SERVO_DESC_PERCENT[]  = "\tSERVO #%\t #'s value must be in range of 0% to 100% ";
  static uint8_t SERVO_DESC_DEGREE[]   = "\tSERVO # \t #'s value must be in range of 0  to 180 degrees";
  static uint8_t INVALID_VALUE[]       = "Invalid value, check HELP for allowed values";
  static uint8_t ADC_DESC[]						 = "ADC <READ or BULK>";
  static uint8_t ADC_DESC_BULK[]			 = "\tADC BULK\t Will print 10 closely gathered ADC readings";
  static uint8_t ADC_DESC_READ[]			 = "\tADC READ\t Will print a single ADC reading";


  static uint8_t COMPILE_TIME[]      = "COMPILE TIME";
  static uint8_t COMPILE_TIME_DESC[] = "\tCOMPILE TIME displays the date and time the source file was compiled";
  static uint8_t COMPILE_LINE[]      = "Compiled at: " __DATE__ " " __TIME__;


    static uint8_t CLEAR_SCREEN[] = "CLR";

   static uint8_t READ_ACCEL[] = "READ ACCEL";
   static uint8_t READ_ACCEL_DESC[] = "\tREAD ACCEL grabs an accelerometer reading and prints out the derivatives of the values";
   static uint8_t READ_MAG[] = "READ MAG";
   static uint8_t READ_MAG_DESC[] = "\tREAD MAG gets a magnetometer reading and display the 3 raw values";
      static uint8_t READ_TEMP[] = "READ TEMP";
     static uint8_t READ_TEMP_DESC[] = "\tREAD TEMP reads and displays the temperature sensor value of the LSM303 MEMS device";

  static uint8_t INVALID_COMMAND[]   = "Invalid Command";

CLI_Driver::CLI_Driver(): USART2_Driver(){};
CLI_Driver::~CLI_Driver(){}

void CLI_Driver::open(){
        // open the USART2 device
        USART2_Driver::open();
        //open the I2C1_Driver
        I2C1_Driver::open();
}


void CLI_Driver::close(){
        USART2_Driver::close();

}


void CLI_Driver::start(){
        USART2_Driver::start();
        I2C1_Driver::startAccel();
        I2C1_Driver::startMag();

}


void CLI_Driver::stop(){
        USART2_Driver::stop();

}


void CLI_Driver::CLI()
{
    //SWSTART
    newLine();
    // pipe out the menu
    sendMenu();

    while(1)
    {
        // if there's a finished command,
        if(ready_buffer)
        {
            // interpret the command
            InterperetBuffer();
            // pipe out the menu
            sendMenu();
            // wipe buffer

            clear_buffer = true;
            ready_buffer = false;
        }
    }
}

void CLI_Driver::getLine()
{
  newLine();
  bufferClear();
  printDoubleCarrot();
}

void CLI_Driver::sendLine(uint8_t* output)
{
  uint32_t i = 0;
  while(output[i] != '\0')
  {
    SendByte(output[i]);
    i++;
  }
    newLine();
}

void CLI_Driver::newLine()
{
  SendByte(0xA);
  SendByte(0xD);
}

void CLI_Driver::sendMenu()
{
  uint8_t Menu_1[] = "Enter a Command below. Use HELP to see a list of commands";
  sendLine(Menu_1);
  newLine();
  printDoubleCarrot();

}

void CLI_Driver::InterperetBuffer()
{
  uint8_t overflow_message[] = "Command too long, truncated to 30 characters";
    bool clr = false;
  bool state = false;
  for (int i = 0; RXBUFFER[i] != '\0'; i++)
  {
    RXBUFFER[i] = toupper(RXBUFFER[i]);
  }
  if(overflow)
    {
    sendLine(overflow_message);
    }
  if(bufferComp(HELP))
    {
    sendHelp();
    }
  else if (bufferComp(COMPILE_TIME))
  {
        sendLine(COMPILE_LINE);
  }
    else if (bufferComp(CLEAR_SCREEN))
    {
        clr = true;
        // ANSI clear screen command
        SendByte('\e');
        SendByte('[');
        SendByte('2');
        SendByte('J');
        // set to first line of screen
        SendByte('\e');
        SendByte('[');
        SendByte('0');
        SendByte(';');
        SendByte('0');
        SendByte('f');
    }
    else if (bufferComp(READ_ACCEL))
    {
      uint8_t printBuffer[20] = {0};
      IMU test(I2C1_Driver::readAccel());
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Raw x: %f", test.getAccel_Raw().ax);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Raw y: %f", test.getAccel_Raw().ay);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Raw z: %f", test.getAccel_Raw().az);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Pilot x: %f", test.getAccel_Pilot().ax);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Pilot y: %f", test.getAccel_Pilot().ay);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_Pilot z: %f", test.getAccel_Pilot().az);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_yaw: %f", test.getAngles().yaw);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_roll: %f", test.getAngles().roll);
          sendLine(printBuffer);
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Accel_pitch: %f", test.getAngles().pitch);
          sendLine(printBuffer);

    }

    else if (bufferComp(READ_MAG))
    {
      Accel temp = I2C1_Driver::readAccel();
      uint8_t printBuffer[20] = {0};
    sprintf(reinterpret_cast<char *>(printBuffer),
      "Mag_Raw x: %f", temp.ax);
          sendLine(printBuffer);
        sprintf(reinterpret_cast<char *>(printBuffer),
      "Mag_Raw y: %f", temp.ay);
          sendLine(printBuffer);
            sprintf(reinterpret_cast<char *>(printBuffer),
      "Mag_Raw z: %f", temp.az);
          sendLine(printBuffer);

    }

    else if (bufferComp(READ_TEMP))
    {
      uint32_t temp = I2C1_Driver::readTemp();
      uint8_t printBuffer[20] = {0};
    sprintf(reinterpret_cast<char *>(printBuffer),
      "temp Raw: %0.2f*C",static_cast<float>(temp)/128 + 20);
          sendLine(printBuffer);
    }
    else
    {
        if(RXBUFFER[0] != '\0')
        sendLine(INVALID_COMMAND);
    }
    // If clear commmand, no newline
    if(clr)
    {
        clr = false;
    }
    else
    {
        newLine();
    }
}

void CLI_Driver::printDoubleCarrot()
{   //Green
    SendByte('\e');
    SendByte('[');
    SendByte('3');
    SendByte('2');
    SendByte('m');
    //STM
    SendByte('S');
    SendByte('T');
    SendByte('M');
    //Clear Formatting
    SendByte('\e');
    SendByte('[');
    SendByte('0');
    SendByte('m');
    //Carrots
  SendByte('>');
  SendByte('>');

}

void CLI_Driver::bufferClear()
{
  // set the buffer to 0;
  for( int i = 0; i < RX_SIZE; i++)
    RXBUFFER[i] = 0x0;
}

bool CLI_Driver::bufferComp(uint8_t* command)
{
  int32_t i = 0;
  // escapes when one of the two strings reads an end of string character
  while((RXBUFFER[i] != '\0') && (command[i] != '\0'))
  {
    if(RXBUFFER[i] != command[i])
      return false;
    i++;
  }
  // if both strings are on an end of string character
  if((RXBUFFER[i] == '\0') && (command[i] == '\0'))
  return true;

  // else return false
  else return false;
}

//bool CLI_Driver::PWMBufferComp(uint8_t* command)
//{
//	char* endptr;
//	int32_t degree = strncmp(reinterpret_cast<const char *>(command),
//											reinterpret_cast<const char *>(RXBUFFER), 4);
//	if(degree == 0 && 0 != isdigit(static_cast<int32_t>(*(RXBUFFER + 6))))
//	{
//		degree = static_cast<uint16_t>(
//						strtol(reinterpret_cast<const char *>(RXBUFFER + 5), &endptr, 10));
//		if(*endptr == '%')
//		{
//			if(degree >= 0 && degree <= 100)
//			{
//				degree = degree * 1.8;
//				TIM4_Driver::SetPeriod(degree);
//				return true;
//			}
//			else
//			{
//				sendLine(INVALID_VALUE);
//				// else return false
//				return false;
//			}
//		}
//		else if(degree <=180 && degree >= 0)
//		{
//			TIM4_Driver::SetPeriod(degree);
//			return true;
//		}

//	}
//		sendLine(INVALID_VALUE);
//		// else return false
//		return false;
//}


void CLI_Driver::sendHelp()
{
  sendLine(HELP_CASE);
  sendLine(HELP_LINE);
  sendLine(COMPILE_TIME);
  sendLine(COMPILE_TIME_DESC);
  sendLine(READ_ACCEL);
  sendLine(READ_ACCEL_DESC);
  sendLine(READ_MAG);
  sendLine(READ_MAG_DESC);
  sendLine(READ_TEMP);
  sendLine(READ_TEMP_DESC);

}


