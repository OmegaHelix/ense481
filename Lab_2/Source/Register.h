#ifndef __REG
#define __REG
/******************************************************************************
* File: Register.h
*
* Description: 	This file holds the basic reigster class, a register should be
*		written to and read. Other classes can expand upon this class.
*
* Member Variables:
*		reg:
*			a constant reference to a volatile unsigned integer
*
* Member Functions:
*	Register(uint32_t):
*		This function Creates an instance of a register from the given
*		unisgned integer as the address.
*
*	void Or(uint32_t Value);
*		This function takes in a value to Or into the register.
*
*	void And(uint32_t Value);
*		This function takes in a value to And with the register.
*
*	uint32_t Read(void);
*		This function returns the value of the register.
*
*
******************************************************************************/
#include "stdlib.h"
#include "stdint.h"
class Register
{
  private:
  // reference to a volatile UInt
  // Can be referenced as register to be written to.
    uint32_t volatile & reg;
  public:
    enum bit {b0 = 0,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,
                        b11,b12,b13,b14,b15,b16,b17,b18,b19,b20,
                        b21,b22,b23,b24,b25,b26,b27,b28,b29,b30,b31};

    Register(uint32_t);
    ~Register();
    void Or(uint32_t);
    void And(uint32_t);
    uint32_t Read(void);
    void Write(uint32_t);
    void Set(enum Register::bit bit);
                void Reset(enum Register::bit bit);
};

#endif


