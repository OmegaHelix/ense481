#ifndef __ADC
#define __ADC

/******************************************************************************
* File: ADC.h
*
* Description: This file holds the basic ADC Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			ADC(uint32_t):
*				This function Creates an instance of an ADC to manipulate.
*
*
*			~ADC();
*			void Or(enum ADC::ADC_, uint32_t);
*			void And(enum ADC::ADC_, uint32_t);
*			void Set(enum ADC::ADC_, uint32_t);
*			uint32_t Read(enum ADC::ADC_);
*			enum ADC_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_ADC.Or(ADC::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class ADC
{
  private:
    // These registers can't be static because they're created as an entity of ADC Drivers
    // Any instance or ADC::func()
    //call will reference these registers for their ADC#.
    Register _SR;
    Register _CR1;
    Register _CR2;
    Register _SMPR1;
    Register _SMPR2;
    Register _JOFR1;
    Register _JOFR2;
    Register _JOFR3;
    Register _JOFR4;
    Register _HTR;
    Register _LTR;
    Register _SQR1;
    Register _SQR2;
    Register _SQR3;
    Register _JSQR;
    Register _JDR1;
    Register _JDR2;
    Register _JDR3;
    Register _JDR4;
    Register _DR;

  public:
                enum ADC_ {SR,CR1,CR2,SMPR1,SMPR2,JOFR1,JOFR2,JOFR3,JOFR4,
                  HTR,LTR,SQR1,SQR2,SQR3,JSQR,JDR1,JDR2,JDR3,JDR4,DR};

                ADC(uint32_t);
                ~ADC();
                void Or(enum ADC::ADC_, uint32_t);
                void And(enum ADC::ADC_, uint32_t);
                void Write(enum ADC::ADC_, uint32_t);
                void Set(enum ADC::ADC_, Register::bit);
                void Reset(enum ADC::ADC_, Register::bit);
                uint32_t Read(enum ADC::ADC_);

};

#endif



