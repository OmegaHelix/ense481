#include "GPIO.h"


GPIO_PORT GPIO::_A(GPIO::_A_BASE);
GPIO_PORT GPIO::_B(GPIO::_B_BASE);
GPIO_PORT GPIO::_C(GPIO::_C_BASE);
GPIO_PORT GPIO::_D(GPIO::_D_BASE);
GPIO_PORT GPIO::_E(GPIO::_E_BASE);
GPIO_PORT GPIO::_F(GPIO::_F_BASE);
GPIO_PORT GPIO::_G(GPIO::_G_BASE);


GPIO::GPIO(){};

GPIO::~GPIO(){};


  void GPIO::Or(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg, uint32_t value)
{
  switch (port)
  {
    case A:
      _A.Or(reg, value);
      break;
    case B:
      _B.Or(reg, value);
      break;
    case C:
      _C.Or(reg, value);
      break;
    case D:
      _D.Or(reg, value);
      break;
    case E:
      _E.Or(reg, value);
      break;
    case F:
      _F.Or(reg, value);
      break;
    case G:
      _G.Or(reg, value);
      break;
  };
};

  void GPIO::And(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg, uint32_t value)
{
  switch (port)
  {
    case A:
      _A.And(reg, value);
      break;
    case B:
      _B.And(reg, value);
      break;
    case C:
      _C.And(reg, value);
      break;
    case D:
      _D.And(reg, value);
      break;
    case E:
      _E.And(reg, value);
      break;
    case F:
      _F.And(reg, value);
      break;
    case G:
      _G.And(reg, value);
      break;
  };
};

  void GPIO::Write(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg, uint32_t value)
{
  switch (port)
  {
    case A:
      _A.Write(reg, value);
      break;
    case B:
      _B.Write(reg, value);
      break;
    case C:
      _C.Write(reg, value);
      break;
    case D:
      _D.Write(reg, value);
      break;
    case E:
      _E.Write(reg, value);
      break;
    case F:
      _F.Write(reg, value);
      break;
    case G:
      _G.Write(reg, value);
      break;
  };
};


void GPIO::Set(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg, Register::bit bit)
{
  switch (port)
  {
    case A:
      _A.Set(reg, bit);
      break;
    case B:
      _B.Set(reg, bit);
      break;
    case C:
      _C.Set(reg, bit);
      break;
    case D:
      _D.Set(reg, bit);
      break;
    case E:
      _E.Set(reg, bit);
      break;
    case F:
      _F.Set(reg, bit);
      break;
    case G:
      _G.Set(reg, bit);
      break;
  };
};


void GPIO::Reset(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg, Register::bit bit)
{
  switch (port)
  {
    case A:
      _A.Reset(reg, bit);
      break;
    case B:
      _B.Reset(reg, bit);
      break;
    case C:
      _C.Reset(reg, bit);
      break;
    case D:
      _D.Reset(reg, bit);
      break;
    case E:
      _E.Reset(reg, bit);
      break;
    case F:
      _F.Reset(reg, bit);
      break;
    case G:
      _G.Reset(reg, bit);
      break;
  };
};


  uint32_t GPIO::Read(enum GPIO::GPIO_ port, enum GPIO_PORT::PORT_ reg)
{
  uint32_t value = 0;
  switch (port)
  {
    case A:
      value = _A.Read(reg);
      break;
    case B:
      value = _B.Read(reg);
      break;
    case C:
      value = _C.Read(reg);
      break;
    case D:
      value = _D.Read(reg);
      break;
    case E:
      value = _E.Read(reg);
      break;
    case F:
      value = _F.Read(reg);
      break;
    case G:
      value = _G.Read(reg);
      break;
  };
  return value;
};


