#include "SPI.h"

SPI::SPI(uint32_t _BASE):   _CR1        (_BASE + 0x00),
                                          _CR2        (_BASE + 0x04),
                                          _SR          (_BASE + 0x08),
                                          _DR          (_BASE + 0x0C),
                                          _CRCPR   (_BASE + 0x10),
                                          _RXCRCR  (_BASE + 0x14),
                                          _TXCRCR  (_BASE + 0x18){};
SPI::~SPI(){};


void SPI::Or(enum SPI::SPI_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.Or(value);
      break;
    case CR2:
      _CR2.Or(value);
      break;
    case SR:
      _SR.Or(value);
      break;
    case DR:
      _DR.Or(value);
      break;
    case CRCPR:
      _CRCPR.Or(value);
      break;
    case RXCRCR:
      _RXCRCR.Or(value);
      break;
    case TXCRCR:
      _TXCRCR.Or(value);
      break;
  };
};
void SPI::And(enum SPI::SPI_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.And(value);
      break;
    case CR2:
      _CR2.And(value);
      break;
    case SR:
      _SR.And(value);
      break;
    case DR:
      _DR.And(value);
      break;
    case CRCPR:
      _CRCPR.And(value);
      break;
    case RXCRCR:
      _RXCRCR.And(value);
      break;
    case TXCRCR:
      _TXCRCR.And(value);
      break;
  };
};

void SPI::Set(enum SPI::SPI_ reg, Register::bit bit)
{
  switch (reg)
  {
    case CR1:
      _CR1.Set(bit);
      break;
    case CR2:
      _CR2.Set(bit);
      break;
    case SR:
      _SR.Set(bit);
      break;
    case DR:
      _DR.Set(bit);
      break;
    case CRCPR:
      _CRCPR.Set(bit);
      break;
    case RXCRCR:
      _RXCRCR.Set(bit);
      break;
    case TXCRCR:
      _TXCRCR.Set(bit);
      break;
  };
};

void SPI::Reset(enum SPI::SPI_ reg, Register::bit bit)
{
    switch (reg)
  {
    case CR1:
      _CR1.Reset(bit);
      break;
    case CR2:
      _CR2.Reset(bit);
      break;
    case SR:
      _SR.Reset(bit);
      break;
    case DR:
      _DR.Reset(bit);
      break;
    case CRCPR:
      _CRCPR.Reset(bit);
      break;
    case RXCRCR:
      _RXCRCR.Reset(bit);
      break;
    case TXCRCR:
      _TXCRCR.Reset(bit);
      break;
  };
};




uint32_t SPI::Read(enum SPI::SPI_ reg)
{
  uint32_t value = 0;
  switch (reg)
  {
    case CR1:
      value = _CR1.Read();
      break;
    case CR2:
      value = _CR2.Read();
      break;
    case SR:
      value = _SR.Read();
      break;
    case DR:
      value = _DR.Read();
      break;
    case CRCPR:
      value = _CRCPR.Read();
      break;
    case RXCRCR:
      value = _RXCRCR.Read();
      break;
    case TXCRCR:
      value = _TXCRCR.Read();
      break;
  };
  return value;
};

void SPI::Write(enum SPI::SPI_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.Write(value);
      break;
    case CR2:
      _CR2.Write(value);
      break;
    case SR:
      _SR.Write(value);
      break;
    case DR:
      _DR.Write(value);
      break;
    case CRCPR:
      _CRCPR.Write(value);
      break;
    case RXCRCR:
      _RXCRCR.Write(value);
      break;
    case TXCRCR:
      _TXCRCR.Write(value);
      break;
  };
};
