#include "Timer.h"



TIMER::TIMER(uint32_t _BASE):   _CR1  (_BASE + 0x00),
                                _CR2  (_BASE + 0x04),
                                _SMCR (_BASE + 0x08),
                                _DIER (_BASE + 0x0C),
                                _SR   (_BASE + 0x10),
                                _EGR  (_BASE + 0x14),
                                _CCMR1(_BASE + 0x18),
                                _CCMR2(_BASE + 0x1C),
                                _CCER (_BASE + 0x20),
                                _CNT  (_BASE + 0x24),
                                _PSC  (_BASE + 0x28),
                                _ARR  (_BASE + 0x2C),
                                _CCR1 (_BASE + 0x34),
                                _CCR2 (_BASE + 0x38),
                                _CCR3 (_BASE + 0x3C),
                                _CCR4 (_BASE + 0x40),
                                _DCR  (_BASE + 0x48),
                                _DMAR (_BASE + 0x4C){};
TIMER::~TIMER(){};


void TIMER::Or(enum TIMER::TIMER_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.Or(value);
      break;
    case CR2:
      _CR2.Or(value);
      break;
    case SMCR:
      _SMCR.Or(value);
      break;
    case DIER:
      _DIER.Or(value);
      break;
    case SR:
      _SR.Or(value);
      break;
    case EGR:
      _EGR.Or(value);
      break;
    case CCMR1:
      _CCMR1.Or(value);
      break;
    case CCMR2:
      _CCMR2.Or(value);
      break;
    case CCER:
      _CCER.Or(value);
      break;
    case CNT:
      _CNT.Or(value);
      break;
    case PSC:
      _PSC.Or(value);
      break;
    case ARR:
      _ARR.Or(value);
      break;
    case CCR1:
      _CCR1.Or(value);
      break;
    case CCR2:
      _CCR2.Or(value);
      break;
    case CCR3:
      _CCR3.Or(value);
      break;
    case CCR4:
      _CCR4.Or(value);
      break;
    case DCR:
      _DCR.Or(value);
      break;
    case DMAR:
      _DMAR.Or(value);
      break;

  };
};
void TIMER::And(enum TIMER::TIMER_ reg, uint32_t value)
{
  switch (reg)
  {
      case CR1:
      _CR1.And(value);
      break;
    case CR2:
      _CR2.And(value);
      break;
    case SMCR:
      _SMCR.And(value);
      break;
    case DIER:
      _DIER.And(value);
      break;
    case SR:
      _SR.And(value);
      break;
    case EGR:
      _EGR.And(value);
      break;
    case CCMR1:
      _CCMR1.And(value);
      break;
    case CCMR2:
      _CCMR2.And(value);
      break;
    case CCER:
      _CCER.And(value);
      break;
    case CNT:
      _CNT.And(value);
      break;
    case PSC:
      _PSC.And(value);
      break;
    case ARR:
      _ARR.And(value);
      break;
    case CCR1:
      _CCR1.And(value);
      break;
    case CCR2:
      _CCR2.And(value);
      break;
    case CCR3:
      _CCR3.And(value);
      break;
    case CCR4:
      _CCR4.And(value);
      break;
    case DCR:
      _DCR.And(value);
      break;
    case DMAR:
      _DMAR.And(value);
      break;
  };
};

void TIMER::Set(enum TIMER::TIMER_ reg, Register::bit bit)
{
  switch (reg)
  {
    case CR1:
      _CR1.Set(bit);
      break;
    case CR2:
      _CR2.Set(bit);
      break;
    case SMCR:
      _SMCR.Set(bit);
      break;
    case DIER:
      _DIER.Set(bit);
      break;
    case SR:
      _SR.Set(bit);
      break;
    case EGR:
      _EGR.Set(bit);
      break;
    case CCMR1:
      _CCMR1.Set(bit);
      break;
    case CCMR2:
      _CCMR2.Set(bit);
      break;
    case CCER:
      _CCER.Set(bit);
      break;
    case CNT:
      _CNT.Set(bit);
      break;
    case PSC:
      _PSC.Set(bit);
      break;
    case ARR:
      _ARR.Set(bit);
      break;
    case CCR1:
      _CCR1.Set(bit);
      break;
    case CCR2:
      _CCR2.Set(bit);
      break;
    case CCR3:
      _CCR3.Set(bit);
      break;
    case CCR4:
      _CCR4.Set(bit);
      break;
    case DCR:
      _DCR.Set(bit);
      break;
    case DMAR:
      _DMAR.Set(bit);
      break;
  };
};

void TIMER::Reset(enum TIMER::TIMER_ reg, Register::bit bit)
{
  switch (reg)
  {
    case CR1:
      _CR1.Reset(bit);
      break;
    case CR2:
      _CR2.Reset(bit);
      break;
    case SMCR:
      _SMCR.Reset(bit);
      break;
    case DIER:
      _DIER.Reset(bit);
      break;
    case SR:
      _SR.Reset(bit);
      break;
    case EGR:
      _EGR.Reset(bit);
      break;
    case CCMR1:
      _CCMR1.Reset(bit);
      break;
    case CCMR2:
      _CCMR2.Reset(bit);
      break;
    case CCER:
      _CCER.Reset(bit);
      break;
    case CNT:
      _CNT.Reset(bit);
      break;
    case PSC:
      _PSC.Reset(bit);
      break;
    case ARR:
      _ARR.Reset(bit);
      break;
    case CCR1:
      _CCR1.Reset(bit);
      break;
    case CCR2:
      _CCR2.Reset(bit);
      break;
    case CCR3:
      _CCR3.Reset(bit);
      break;
    case CCR4:
      _CCR4.Reset(bit);
      break;
    case DCR:
      _DCR.Reset(bit);
      break;
    case DMAR:
      _DMAR.Reset(bit);
      break;
  };
};




uint32_t TIMER::Read(enum TIMER::TIMER_ reg)
{
  uint32_t value = 0;
  switch (reg)
  {
    case CR1:
      value = _CR1.Read();
      break;
    case CR2:
      value = _CR2.Read();
      break;
    case SMCR:
      value = _SMCR.Read();
      break;
    case DIER:
      value = _DIER.Read();
      break;
    case SR:
      value = _SR.Read();
      break;
    case EGR:
      value = _EGR.Read();
      break;
    case CCMR1:
      value = _CCMR1.Read();
      break;
    case CCMR2:
      value = _CCMR2.Read();
      break;
    case CCER:
      value = _CCER.Read();
      break;
    case CNT:
      value = _CNT.Read();
      break;
    case PSC:
      value = _PSC.Read();
      break;
    case ARR:
      value = _ARR.Read();
      break;
    case CCR1:
      value = _CCR1.Read();
      break;
    case CCR2:
      value = _CCR2.Read();
      break;
    case CCR3:
      value = _CCR3.Read();
      break;
    case CCR4:
      value = _CCR4.Read();
      break;
    case DCR:
      value = _DCR.Read();
      break;
    case DMAR:
      value = _DMAR.Read();
      break;
  };
  return value;
};

void TIMER::Write(enum TIMER::TIMER_ reg, uint32_t value)
{
  switch (reg)
  {
      case CR1:
      _CR1.Write(value);
      break;
    case CR2:
      _CR2.Write(value);
      break;
    case SMCR:
      _SMCR.Write(value);
      break;
    case DIER:
      _DIER.Write(value);
      break;
    case SR:
      _SR.Write(value);
      break;
    case EGR:
      _EGR.Write(value);
      break;
    case CCMR1:
      _CCMR1.Write(value);
      break;
    case CCMR2:
      _CCMR2.Write(value);
      break;
    case CCER:
      _CCER.Write(value);
      break;
    case CNT:
      _CNT.Write(value);
      break;
    case PSC:
      _PSC.Write(value);
      break;
    case ARR:
      _ARR.Write(value);
      break;
    case CCR1:
      _CCR1.Write(value);
      break;
    case CCR2:
      _CCR2.Write(value);
      break;
    case CCR3:
      _CCR3.Write(value);
      break;
    case CCR4:
      _CCR4.Write(value);
      break;
    case DCR:
      _DCR.Write(value);
      break;
    case DMAR:
      _DMAR.Write(value);
      break;
  };
};
