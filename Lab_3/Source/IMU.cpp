#include "IMU.h"
//#include <iostream>

/**
 * @brief Construct a new IMU::IMU object
 *
 * @details Accelerometer is defaulted to {0,0,0}
 *
 */
IMU::IMU(){}

/**
 * @brief Construct a new IMU::IMU object from 3 floats.
 *
 * @details Allows to manually seed IMU with initial data reading.
 *
 * @param ax as the x-axis reading
 * @param ay as the y-axis reading
 * @param az as the z-axis reading
 */
IMU::IMU(float ax, float ay, float az)
{
    Accelerometer_Raw = {ax,ay,az};
    calculateAll();
}

/**
 * @brief Construct a new IMU::IMU object
 *
 * @details Allows to manually seed IMU with initial data reading.
 *
 * @param accelData as the accelerometer to base the IMU base Raw off.
 */
IMU::IMU(Accel accelData)
{
    Accelerometer_Raw = accelData;
    calculateAll();
}


/**
 * @brief Get the Accel object with raw values
 *
 * @return Accel
 */
Accel IMU::getAccel_Raw()
{
    return Accelerometer_Raw;
}

/**
 * @brief Get the Accel object with g values.
 * multiply by 9.8m/s^2 after recieveing
 *
 * @return Accel
 */
Accel IMU::getAccel_Pilot()
{
    return Accelerometer_Pilot;
}

Angles IMU::getAngles()
{
    return Angle;
}


bool IMU::getLinear_Acceleration()
{
    return Linear_Acceleration;
}

/**
 * @brief Get the gravity value
 *
 * @return float
 */
float IMU::getGravity()
{
    return GRAVITY;
}

/**
 * @brief Get the Norm value
 *
 * @return float
 */
float IMU::getNorm()
{
    return Norm;
}



/**
 * @brief seed fake accelerometer data
 *
 * @param ax as the x-axis reading
 * @param ay as the y-axis reading
 * @param az as the z-axis reading
 * @return true
 * @return false
 */
bool IMU::setFakeAccel(float ax, float ay, float az)
{
    Accelerometer_Raw.ax = ax;
    Accelerometer_Raw.ay = ay;
    Accelerometer_Raw.az = az;
    calculateAll();
    return true;
}

bool IMU::calculateAll()
{
    calculateAccelerometer_Pilot();
    calculateAllAngles();
    calculateNorm();
    return true;
}

bool IMU::calculateAccelerometer_Pilot()
{
    Accelerometer_Pilot.ax = Accelerometer_Raw.ax / GRAVITY;
    Accelerometer_Pilot.ay = Accelerometer_Raw.ay / GRAVITY;
    Accelerometer_Pilot.az = Accelerometer_Raw.az / GRAVITY;
    return true;
}

bool IMU::calculateAllAngles()
{
    calculateRollAngle();
    calculatePitchAngle();
    return true;
}

bool IMU::calculatePitchAngle()
{
    Angle.pitch = atan2f( - Accelerometer_Pilot.ax,
                        hypotf(Accelerometer_Pilot.ay,
                              Accelerometer_Pilot.az)
                        ) * (180.f/PI);
    return true;
}

bool IMU::calculateRollAngle()
{

    Angle.roll = atan2f(Accelerometer_Pilot.ay,
                        Accelerometer_Pilot.az) * (180.f/PI);
    return true;
}

bool IMU::calculateNorm()
{
    Norm = sqrtf( Accelerometer_Pilot.ax * Accelerometer_Pilot.ax
                + Accelerometer_Pilot.ay * Accelerometer_Pilot.ay
                + Accelerometer_Pilot.az * Accelerometer_Pilot.az);
    if(roundf(Norm) != 1)
        //std::cout << "Linear Acceleration Detected\nNorm Length: " << Norm << std::endl;
        Linear_Acceleration = true;
    else
        Linear_Acceleration = false;

    return true;


}
