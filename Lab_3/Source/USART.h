#ifndef __USART
#define __USART

/******************************************************************************
* File: USART.h
*
* Description: This file holds the basic USART Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			USART(uint32_t):
*				This function Creates an instance of an USART to manipulate.
*
*
*			~USART();
*			void Or(enum USART::USART_, uint32_t);
*			void And(enum USART::USART_, uint32_t);
*			void Set(enum USART::USART_, uint32_t);
*			uint32_t Read(enum USART::USART_);
*			enum USART_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_USART.Or(USART::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class USART
{
  private:
    // These registers can't be static because they're created as an entity of USART Drivers
    // Any instance or USART::func()
    //call will reference these registers for their usart#.
    Register _SR;
    Register _DR;
    Register _BRR;
    Register _CR1;
    Register _CR2;
    Register _CR3;
    Register _GTPR;

  public:
                enum USART_ {SR,DR,BRR,CR1,CR2,CR3,GTPR};
                USART(uint32_t);
                ~USART();
                void Or(enum USART::USART_, uint32_t);
                void And(enum USART::USART_, uint32_t);
                void Write(enum USART::USART_, uint32_t);
                void Set(enum USART::USART_, Register::bit);
                void Reset(enum USART::USART_, Register::bit);
                uint32_t Read(enum USART::USART_);

};

#endif



