#ifndef __SPI_Driver
#define __SPI_Driver

#include "SPI.h"
#include "GPIO.h"
#include "RCC.h"
#include <queue>

// This driver is just for USART 2
// Entire class is static to allow easier usage with C IRQ's
class SPI_Driver
{
  private:
    // SPI register for the driver to utilize
    static uint8_t RXBUFFER[];
    static const uint32_t RX_SIZE;
    static uint32_t RX_FRONT;
    static uint32_t RX_BACK;
    static SPI SPI2;
  public:

    // constant for SPI
    static const uint32_t _SPI;

    //Constructor and Destructor
    SPI_Driver();
    ~SPI_Driver();

    //Driver functions
    static void open();
    static void close();
    static void start();
    static void stop();

    //Usage Functions
    static void SendByte(uint8_t);
    static uint8_t GetByte();
    static bool getPacket(uint8_t *, int32_t size = 20);
    static void getResponse();
    static void resetRXBUFFER();



    // static class interrupt function to be called from C IRQ handler
                static void SPI2_IRQHandler(void);

  // befriend class CLI_Driver to let it have complete inheritance
  friend class CLI_Driver;
};

//interrupt data

extern "C" void SPI2_IRQHandler(void);


#endif



