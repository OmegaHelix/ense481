#include "RCC.h"
#include "GPIO.h"
#include "USART2_Driver.h"
#include "CLI_Driver.h"
#include "I2C1_Driver.h"
void delay (int a)
{
 volatile int i,j;
 for (i=0 ; i < a ; i++)
 {
  j++;
 }
 return;
}

int main(void)
{
volatile uint32_t	dly;

    RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPCEN);
    // Clear GPIOC Pin Configurations for Pin C8 and C9
  GPIO::And(GPIO::C, GPIO_PORT::CRH,~((0xFu << 0)  | //Pin C8
                                        (0xFu << 4) )); //Pin C9
  //Set GPIOC Pin 8 and 9 configuration
  GPIO::Or(GPIO::C, GPIO_PORT::CRH,((0x3 << 0) | //Pin C8
                                     (0x3 << 4))); //Pin C9

    CLI_Driver myCLI;
    myCLI.open();
    myCLI.start();
    myCLI.CLI();
}


