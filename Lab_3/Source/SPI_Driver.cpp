#include "SPI_Driver.h"


const uint32_t SPI_Driver::_SPI = 0x40003800;

//SPI Registers
SPI SPI_Driver::SPI2(_SPI); 
const uint32_t SPI_Driver::RX_SIZE = 256;
uint32_t SPI_Driver::RX_FRONT = 0;
uint32_t SPI_Driver::RX_BACK = 0;

//Character buffer for the SPI buffers
uint8_t  SPI_Driver::RXBUFFER[SPI_Driver::RX_SIZE] = {0};


SPI_Driver::SPI_Driver(){}
SPI_Driver::~SPI_Driver(){}


// sendByte is still in polling mode
void SPI_Driver::SendByte(uint8_t byte)
{
  while(!(SPI2.Read(SPI::SR) & (0x2)))
    {
      asm("");
    };

  SPI2.Write(SPI::DR, byte);
}

//GetByte is in interrupt 
uint8_t SPI_Driver::GetByte()
{
  int i = 0;
  while((i++ < 500) && (!(SPI2.Read(SPI::SR) & (0x1)))){};
    return SPI2.Read(SPI::DR);
}

void SPI_Driver::open()
{
  // 1. Enable RCC2 GPIOB AFIO and SPI2 Clocks
  RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPBEN);
  RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_AFIOEN);
  RCC2::Or(RCC2::APB1ENR, RCC2::_APB1ENR_SPI2EN);

  // 2. Configure GPIO pins
  
  //MOSI Pin
  //AFIO push pull 10 11
  //Clear GPIOB port 15
  GPIO::And(GPIO::B, GPIO_PORT::CRH, ~(0xF << 28));
  //Set GPIOB port 15 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRH, 0xB << 28);

  // MISO Pin
  //floating input 01 00
  //Clear GPIOB port 14
  GPIO::And(GPIO::B, GPIO_PORT::CRH, ~(0xF << 24));
  //Set GPIOB port 14 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRH, 0x4 << 24);
  
  //SCLK Pin
  //AFIO Push-pull 10 11
  //Clear GPIOB port 13
  GPIO::And(GPIO::B, GPIO_PORT::CRH, ~(0xF << 20));
  //Set GPIOB port 13 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRH, 0xB << 20);
  
  //NSS Pin
  //General Purpose output Push-pull 00 11
  //Clear GPIOB port 12
  GPIO::And(GPIO::B, GPIO_PORT::CRH, ~(0xF << 16));
  //Set GPIOB port 12 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRH, 0x3 << 16);  
  
   // IRQ Pin
  //pull up 00 10
  //Clear GPIOB port 11
  GPIO::And(GPIO::B, GPIO_PORT::CRH, ~(0xF << 12));
  //Set GPIOB port 12 configuration
  GPIO::Or(GPIO::B, GPIO_PORT::CRH, 0x20 << 12);  



// 3. Configure Registers


 // Set the NSS  High
GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b12);

// Set CR1_SSM and SSI
  SPI2.Set(SPI::CR1, Register::b9); // SSM
  SPI2.Set(SPI::CR1, Register::b8); // SSI

// Reset CR2_SSOE
  SPI2.Set(SPI::CR2, Register::b2); //SSOE

// Enable DMA for faster data transfers
  SPI2.Set(SPI::CR2, Register::b0); //TXDMA
  SPI2.Set(SPI::CR2, Register::b1); //RXDMA

// Enable RX Interrupt
  SPI2.Set(SPI::CR2, Register::b6); //RXNEIE


// Set CR1_BR_0 and BR_2 and Reset BR_1
  SPI2.Reset(SPI::CR1, Register::b3);  // BR_0
  SPI2.Reset(SPI::CR1, Register::b5);  // BR_2
  SPI2.Set(SPI::CR1, Register::b4);     // BR_1

// Set CR1_MSTR
  SPI2.Set(SPI::CR1, Register::b2); // MSTR

// Set CR1_SPE
  SPI2.Set(SPI::CR1, Register::b6); // SPE
  
  // enable interrupt in NVIC ISER
  // NVIC ISER base address is 0xE000E100
  // there is 3 registers in NVIC ISER (0,1,2)
  // each register is at a 4 byte offset
  // SPI2 is interrupt #36
        Register NVIC_ISER1(0xE000E104);
        NVIC_ISER1.Set(Register::b4);
}

void SPI_Driver::close()
{
}

void SPI_Driver::start()
{
  // Set the NSS  Low
  GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b12);
}

void SPI_Driver::stop()
{
  // Set the NSS  High
  GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b12);
}
// input is the beginning of a char array, and the size of the array
// Gets a packet from the Program's RX Buffer and stores it in packet_holder
// Assume size is 20, as that's the maximum for packets.
// A larger size can be given, but only 20 at most will be used 
// by definition of the incoming packet.


void SPI_Driver::getResponse(){
  // wait for the IRQ to go high
 //  while(!(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << 11))){}
     // while it's high, read data
  while(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << 11))
  {
    SendByte(0xFF);
  }
}


bool SPI_Driver::getPacket(uint8_t packet_holder[], int32_t size){
  bool overRead = false;
  uint8_t packet_size;
  // loop until we find a new packet
  while(RXBUFFER[RX_BACK] != 0x10 && RXBUFFER[RX_BACK] != 0x20
    && RXBUFFER[RX_BACK] != 0x40 && RXBUFFER[RX_BACK] != 0x80)
  {
      RX_BACK++;
          if(RX_BACK >= RX_SIZE){
              RX_BACK = 0;
              if(overRead){
                return false;
              }
              else {
                overRead = true;
              }
      }
  }
  // read the header of the packet
    for(auto i = 0; i < 4; i++){
      // using temp for debug
      uint8_t temp = RXBUFFER[RX_BACK++];
      packet_holder[i] = temp;
      if(RX_BACK >= RX_SIZE){
        RX_BACK = 0;
      }
      // If a packet in the header is bad, check the next byte.
      // We want to discard any bad data
      if((packet_holder[i] == 0xFE ) || (packet_holder[i] == 0xFF ))
      {
        i--;
      }
  }
    //get the Packet Size Byte
    packet_size = RXBUFFER[RX_BACK - 1];
  
    if(packet_size  == 0x20 || packet_size  == 0x80 || packet_size  == 0x40){
      RX_BACK--;
      packet_size = 0;
    }
   
    // Bit 7 is the "More" bit, mask it out for the number of Bytes in the packet
    // Subtract 4 from size to account for the header
    for(auto i = 0; (i < (packet_size & ~(0x1 << 7))) && (i < (size - 4)); i++)
    {
        // Load the data members into the packet
        // start with Byte 4 since [0...3] are the packet header
        packet_holder[i+4] = RXBUFFER[RX_BACK++];
    }
    // At this point, packet_holder holds the latest packet from the RX Buffer
    return true;
}


void SPI_Driver::resetRXBUFFER(){
  for (auto a : RXBUFFER){
    a = 0;
  }
  RX_FRONT = 0;
  RX_BACK = 0;
}


void SPI_Driver::SPI2_IRQHandler(void)
{
  // If a RX flag triggered the Interrupt handler
  if(SPI2.Read(SPI::SR) & 0x1)
  {
    // Store the byte regardless
    RXBUFFER[RX_FRONT++] = SPI2.Read(SPI::DR);
    if(RX_FRONT >= RX_SIZE)
        RX_FRONT = 0;
  }
}
void SPI2_IRQHandler(void)
{
        SPI_Driver::SPI2_IRQHandler();
}


