#pragma once
/**
 * @brief       Accelerometer 3-tuple
 * @details     This struct holds 3 floats to represent three axis
 *              of acceleration. Their values are in m/s^2.
 * @author      Dakota Fisher
 * @date        2019
 * @version     1.0
 */
struct Accel
{
    float ax; // ax is the x-axis acceleration value in m/s^2
    float ay; // ay is the y-axis acceleration value in m/s^2
    float az; // az is the z-axis acceleration value in m/s^2
};
