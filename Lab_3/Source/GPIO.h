#ifndef __GPIO
#define __GPIO

/******************************************************************************
* File: GPIO.h
*
* Description: This file holds the basic GPIO Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			GPIO(uint32_t):
*				This function Creates an instance of an GPIO to manipulate.
*
*
*			~GPIO();
*			void Or(enum GPIO::GPIO_, uint32_t);
*			void And(enum GPIO::GPIO_, uint32_t);
*			void Write(enum GPIO::GPIO_, uint32_t);
*			void Set(enum GPIO::GPIO_, uint32_t);
*			void Reset(enum GPIO::GPIO_, uint32_t);
*			uint32_t Read(enum GPIO::GPIO_);
*			enum GPIO_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_GPIO.Or(GPIO::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "GPIO_PORT.h"
class GPIO
{
  private:
    //GPIO_PORT's base adddresses, no real way to avoid magic numbers here.
    static const uint32_t _A_BASE = 0x40010800;
    static const uint32_t _B_BASE = 0x40010C00;
    static const uint32_t _C_BASE = 0x40011000;
    static const uint32_t _D_BASE = 0x40011400;
    static const uint32_t _E_BASE = 0x40011800;
    static const uint32_t _F_BASE = 0x40011C00;
    static const uint32_t _G_BASE = 0x40012000;
    // Registers are static because there is only one version of them
    // Any instance or GPIO::func() call will reference these same Registers.
    static  GPIO_PORT _A;
    static  GPIO_PORT _B;
    static  GPIO_PORT _C;
    static  GPIO_PORT _D;
    static  GPIO_PORT _E;
    static  GPIO_PORT _F;
    static  GPIO_PORT _G;
public:
  enum GPIO_ {A,B,C,D,E,F,G};
  GPIO();
  ~GPIO();
  static void Or(enum GPIO::GPIO_, enum GPIO_PORT::PORT_, uint32_t);
  static void And(enum GPIO::GPIO_, enum GPIO_PORT::PORT_, uint32_t);
  static void Write(enum GPIO::GPIO_, enum GPIO_PORT::PORT_, uint32_t);
  static void Set(enum GPIO::GPIO_, enum GPIO_PORT::PORT_, Register::bit);
        static void Reset(enum GPIO::GPIO_, enum GPIO_PORT::PORT_, Register::bit);
  static uint32_t Read(enum GPIO::GPIO_, enum GPIO_PORT::PORT_);

};

#endif

