#ifndef __I2C
#define __I2C

/******************************************************************************
* File: I2C.h
*
* Description: This file holds the basic I2C Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			I2C(uint32_t):
*				This function Creates an instance of an I2C to manipulate.
*
*
*			~I2C();
*			void Or(enum I2C::I2C_, uint32_t);
*			void And(enum I2C::I2C_, uint32_t);
*			void Set(enum I2C::I2C_, uint32_t);
*			uint32_t Read(enum I2C::I2C_);
*			enum I2C_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_I2C.Or(I2C::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class I2C
{
  private:
    // These registers can't be static because they're created as an entity of I2C Drivers
    // Any instance or I2C::func()
    //call will reference these registers for their usart#.
    Register _CR1;
    Register _CR2;
    Register _OAR1;
    Register _OAR2;
    Register _DR;
    Register _SR1;
    Register _SR2;
      Register _CCR;
    Register _TRISE;


  public:
                enum I2C_ {CR1,CR2,OAR1,OAR2,DR,SR1,SR2,CCR,TRISE};
                I2C(uint32_t);
                ~I2C();
                void Or(enum I2C::I2C_, uint32_t);
                void And(enum I2C::I2C_, uint32_t);
                void Write(enum I2C::I2C_, uint32_t);
                void Set(enum I2C::I2C_, Register::bit);
                void Reset(enum I2C::I2C_, Register::bit);
                uint32_t Read(enum I2C::I2C_);

};

#endif



