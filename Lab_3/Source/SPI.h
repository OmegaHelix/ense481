#ifndef __SPI
#define __SPI

/******************************************************************************
* File: SPI.h
*
* Description: This file holds the basic SPI Register structure
*
* Member Variables:
*			reg:
*				a constant reference to a volatile unsigned integer
*
* Member Functions:
*			SPI(uint32_t):
*				This function Creates an instance of an SPI to manipulate.
*
*
*			~SPI();
*			void Or(enum SPI::SPI_, uint32_t);
*			void And(enum SPI::SPI_, uint32_t);
*			void Set(enum SPI::SPI_, uint32_t);
*			uint32_t Read(enum SPI::SPI_);
*			enum SPI_
*				Contains the following enumerated values to allow for easier
*				reuse of functions and readability. To be used in function
*				calls such as Reg_SPI.Or(SPI::CR, 0x1);
*
*				CR, CFGR, CIR, APB2RSTR, APB1RSTR,
*				AHBENR, APB2ENR, APB1ENR, BDCR.
*
*
*
*
*
******************************************************************************/
#include "Register.h"
class SPI
{
  private:
    // These registers can't be static because they're created as an entity of SPI Drivers
    // Any instance or SPI::func()
    //call will reference these registers for their SPI#.
Register _CR1;
Register _CR2;
Register _SR;
Register _DR;
Register _CRCPR; 
Register _RXCRCR;
Register _TXCRCR;

  public:
                enum SPI_ {CR1,CR2,SR,DR,CRCPR,RXCRCR,TXCRCR};
                SPI(uint32_t);
                ~SPI();
                void Or(enum SPI::SPI_, uint32_t);
                void And(enum SPI::SPI_, uint32_t);
                void Write(enum SPI::SPI_, uint32_t);
                void Set(enum SPI::SPI_, Register::bit);
                void Reset(enum SPI::SPI_, Register::bit);
                uint32_t Read(enum SPI::SPI_);

};

#endif



