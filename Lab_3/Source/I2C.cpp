#include "I2C.h"

I2C::I2C(uint32_t _BASE):
    _CR1   (_BASE + 0x00),
    _CR2   (_BASE + 0x04),
    _OAR1  (_BASE + 0x08),
    _OAR2  (_BASE + 0x0C),
    _DR    (_BASE + 0x10),
    _SR1   (_BASE + 0x14),
    _SR2   (_BASE + 0x18),
    _CCR   (_BASE + 0x1C),
    _TRISE (_BASE + 0x20){};
I2C::~I2C(){};


void I2C::Or(enum I2C::I2C_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.Or(value);
      break;
    case CR2:
      _CR2.Or(value);
      break;
    case OAR1:
      _OAR1.Or(value);
      break;
    case OAR2:
      _OAR2.Or(value);
      break;
    case DR:
      _DR.Or(value);
      break;
    case SR1:
      _SR1.Or(value);
      break;
    case SR2:
      _SR2.Or(value);
      break;
        case CCR:
      _CCR.Or(value);
      break;
        case TRISE:
      _TRISE.Or(value);
      break;
  };
};
void I2C::And(enum I2C::I2C_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.And(value);
      break;
    case CR2:
      _CR2.And(value);
      break;
    case OAR1:
      _OAR1.And(value);
      break;
    case OAR2:
      _OAR2.And(value);
      break;
    case DR:
      _DR.And(value);
      break;
    case SR1:
      _SR1.And(value);
      break;
    case SR2:
      _SR2.And(value);
      break;
        case CCR:
      _CCR.And(value);
      break;
        case TRISE:
      _TRISE.And(value);
      break;
  };
};

void I2C::Set(enum I2C::I2C_ reg, Register::bit bit)
{
  switch (reg)
  {
    case CR1:
      _CR1.Set(bit);
      break;
    case CR2:
      _CR2.Set(bit);
      break;
    case OAR1:
      _OAR1.Set(bit);
      break;
    case OAR2:
      _OAR2.Set(bit);
      break;
    case DR:
      _DR.Set(bit);
      break;
    case SR1:
      _SR1.Set(bit);
      break;
    case SR2:
      _SR2.Set(bit);
      break;
        case CCR:
      _CCR.Set(bit);
      break;
        case TRISE:
      _TRISE.Set(bit);
      break;
  };
};

void I2C::Reset(enum I2C::I2C_ reg, Register::bit bit)
{
  switch (reg)
  {
    case CR1:
      _CR1.Reset(bit);
      break;
    case CR2:
      _CR2.Reset(bit);
      break;
    case OAR1:
      _OAR1.Reset(bit);
      break;
    case OAR2:
      _OAR2.Reset(bit);
      break;
    case DR:
      _DR.Reset(bit);
      break;
    case SR1:
      _SR1.Reset(bit);
      break;
    case SR2:
      _SR2.Reset(bit);
      break;
        case CCR:
      _CCR.Reset(bit);
      break;
        case TRISE:
      _TRISE.Reset(bit);
      break;
  };
};




uint32_t I2C::Read(enum I2C::I2C_ reg)
{
  uint32_t value = 0;
  switch (reg)
  {
    case CR1:
      value = _CR1.Read();
      break;
    case CR2:
      value = _CR2.Read();
      break;
    case OAR1:
      value = _OAR1.Read();
      break;
    case OAR2:
      value = _OAR2.Read();
      break;
    case DR:
      value = _DR.Read();
      break;
    case SR1:
      value = _SR1.Read();
      break;
    case SR2:
      value = _SR2.Read();
      break;
    case CCR:
      value = _CCR.Read();
      break;
    case TRISE:
      value = _TRISE.Read();
      break;

  };
  return value;
};

void I2C::Write(enum I2C::I2C_ reg, uint32_t value)
{
  switch (reg)
  {
    case CR1:
      _CR1.Write(value);
      break;
    case CR2:
      _CR2.Write(value);
      break;
    case OAR1:
      _OAR1.Write(value);
      break;
    case OAR2:
      _OAR2.Write(value);
      break;
    case DR:
      _DR.Write(value);
      break;
    case SR1:
      _SR1.Write(value);
      break;
    case SR2:
      _SR2.Write(value);
      break;
        case CCR:
      _CCR.Write(value);
      break;
        case TRISE:
      _TRISE.Write(value);
      break;
  };
};
