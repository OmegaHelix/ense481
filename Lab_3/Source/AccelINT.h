#pragma once
/**
 * @brief       Accelerometer 3-tuple
 * @details     This struct holds 3 ints to represent three axis
 *              of acceleration. Their values are in m/s^2.
 * @author      Dakota Fisher
 * @date        2019
 * @version     1.0
 */
struct AccelINT
{
    int ax; // ax is the x-axis acceleration value in m/s^2
    int ay; // ay is the y-axis acceleration value in m/s^2
    int az; // az is the z-axis acceleration value in m/s^2
};
