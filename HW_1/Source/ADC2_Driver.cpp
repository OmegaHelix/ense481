#include "ADC2_Driver.h"
#include "LED_Driver.h"
#include "USART2_Driver.h"
const uint32_t ADC1_Driver::ADC1_BASE = 0x40012400;
ADC ADC1_Driver::ADC1(ADC1_BASE);
uint16_t ADC1_Driver::data = 0;
bool ADC1_Driver::ADC1_data_ready = false;
	


ADC1_Driver::ADC1_Driver(){};
ADC1_Driver::~ADC1_Driver(){};
	
	void ADC1_Driver::open()
	{
		// Enable the AFIO clock
		RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_AFIOEN);
		// Enable the GPIOC Clock
		RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPCEN);
		//Enable the ADC1 clock
		RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_ADC1EN);
		// configure GPIOC Pin 0 to floating input
		GPIO::And(GPIO::C, GPIO_PORT::CRL, ~(0xFu));
		GPIO::Or(GPIO::C, GPIO_PORT::CRL, 0x4u);
		
		//use channel 10 in sequence 1
		ADC1.Write(ADC::SQR3, 0xAu);
		
		//Set to 55.5 cycles
		ADC1.Write(ADC::SMPR1, 0x5u);

		//EOCIE
		ADC1.Set(ADC::CR1, Register::b5);
		
		//SCAN MODE
		ADC1.Set(ADC::CR1, Register::b8);
		
		//External triggering
		ADC1.Set(ADC::CR2, Register::b20);
		
		//EXTSEL = 111 (SWSTART external trigger)
		ADC1.Set(ADC::CR2, Register::b17);
		ADC1.Set(ADC::CR2, Register::b18);
		ADC1.Set(ADC::CR2, Register::b19);
		
		//ADCON
		ADC1.Set(ADC::CR2, Register::b0); 
		
		//ADC Reset Calibration
		ADC1.Set(ADC::CR2, Register::b3);
		
		//wait for reset calibration to go low
		while((ADC1.Read(ADC::CR2) & (0x1u << 3))){};

		//ADC Enable Calibration
		ADC1.Set(ADC::CR2, Register::b2);
		
		//wait for enable calibration to go low
		while((ADC1.Read(ADC::CR2) & (0x1u << 2))){};
		


	// Disable interrupt in NVIC ICER
	// NVIC ICER base address is 0xE000E180
	// there is 3 registers in NVIC ICER(0,1,2)
	// each register is at a 4 byte offset
	// USART2 is interrupt #38
        Register NVIC_ISER0(0xE000E100);
        NVIC_ISER0.Set(Register::b18);
			
		//SWSTART 
		ADC1.Set(ADC::CR2, Register::b22);		
	}		
	
	void ADC1_Driver::close(){};
	
	
	void ADC1_Driver::start(){};
	
	
	void ADC1_Driver::stop(){};

	float ADC1_Driver::getVoltage()
	{
		return static_cast<float>(data) * .824;
	}
		
	void ADC1_Driver::ADC1_2_IRQHandler()
	{
		//1 clear the EOC bit
		ADC1.Reset(ADC::SR, Register::b2);
		//2 Read the data
		data = static_cast<uint16_t>(ADC1.Read(ADC::DR));
		
		// start a new converstion
		ADC1.Set(ADC::CR2, Register::b22);	
		
		ADC1_data_ready = true;

		
	}
		
	
	void ADC1_2_IRQHandler(void) 
	{
		ADC1_Driver::ADC1_2_IRQHandler();
	} 
