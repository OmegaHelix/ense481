#include "Register16.h"

Register16::Register16(uint16_t Reg) : 
	reg(*reinterpret_cast<uint16_t volatile*>(Reg))
{}

void Register16::Or(uint16_t Value)
{
	reg |= Value;
}

void Register16::And(uint16_t Value)
{
	reg &= Value;
}

uint16_t Register16::Read(void)
{
	return reg;
}

void Register16::Write(uint16_t Write)
{
	reg = Write;
}

void Register16::Set(enum Register16::bit bit)
{
        reg |= (0x1 << bit);
}

void Register16::Reset(enum Register16::bit bit)
{
        reg &= ~(0x1 << bit);
}

Register16::~Register16()
{
}
