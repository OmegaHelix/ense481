#ifndef __USART2_Driver
#define __USART2_Driver

#include "USART.h"
#include "GPIO.h"
#include "RCC.h"

// This driver is just for USART 2
// Entire class is static to allow easier usage with C IRQ's
class USART2_Driver
{
	private:
		// USART2 register for the driver to utilize
    static USART USART2;
		
		//Transmission and receiving buffer for message logic
    static uint8_t TXBUFFER[];
		static uint8_t RXBUFFER[];

		static int32_t rx_in_index;
		static int32_t tx_in_index;
		static int32_t rx_out_index;
		static int32_t tx_out_index;

		static const uint32_t RX_SIZE;
		static const uint32_t TX_SIZE;

		static const uint32_t TXE;
		static const uint32_t RXNE;

		static const uint8_t ENTER;
		static const uint8_t BKSP;
        static const uint8_t CTRL_BKSP;

		static bool volatile ready_buffer;
		static bool volatile clear_buffer;
		static bool volatile overflow;
	public:
		
		// constant for USART2
		static const uint32_t _USART2;

		//Constructor and Destructor
		USART2_Driver();
		~USART2_Driver();

		//Driver functions
		static void open();
		static void close();
		static void start();
		static void stop();
		
		//Usage Functions
		static void SendByte(uint8_t);
		static uint8_t GetByte();

		// static class interrupt function to be called from C IRQ handler
                static void USART2_IRQHandler(void);

	// befriend class CLI_Driver to let it have complete inheritance
	friend class CLI_Driver;
};

//interrupt data

extern "C" void USART2_IRQHandler(void);


#endif



