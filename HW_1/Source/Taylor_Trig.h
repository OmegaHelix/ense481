#ifndef __TAYLOR_TRIG
#define __TAYLOR_TRIG
short _sin_t(short y);
short _cos_t(short y);
short sin_t(short x);
short cos_t(short x);
#endif