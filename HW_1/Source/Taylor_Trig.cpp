/**
*
*	_sin_t and _cos_t are renamed _sin and _cos
* functions from Crenshaw Ch5 pp118-119
* to allow multiple implementations in the same project
*
*	The following functions implement the Taylor series
* hence, the postcursor <sin/cos>_t
*/

short _sin_t(short y){
	static short s1 = 0x6488;
	static short s3 = 0x2958;
	static short s5 = 0x51a;
	static short s7 = 0x4d;
	long z, prod, sum;
	z = ((long)y * y) >> 12;
	prod = (z * s7) >> 16;
	sum = s5 - prod;
	prod = (z * sum) >> 16;
	sum = s3 - prod;
	prod = (z * sum) >> 16;
	sum = s1 - prod;
	// for better accuracy, round here
	return (short)((y * sum) >> 13);
}


	short _cos_t(short y){
	static short c0 = 0x7fff;
	static short c2 = 0x4ef5;
	static short c4 = 0x103e;
	static short c6 = 0x156;
	long z, prod, sum;
	z = ((long)y * y) >> 12;
	prod = (z * c6) >> 16;
	sum = c4 - prod;
		prod = (z * sum) >> 16;
	sum = c2 - prod;
	// for better accuracy, round here
	prod = (z * sum) >> 15;// ; note, not 16
	return (short)(c0 - prod);
}
	

short sin_t(short x){
	short n = (x + 0x2000) >> 14;
	x -= n * 0x4000;
	switch(n){
		case 0:
			return _sin_t(x);
		case 1:
			return _cos_t(x);
		case 2:
			return - _sin_t(x);
		case 3:
			return - _cos_t(x);
		default:
			return 0;
	};
}

short cos_t(short x){
return sin_t(x + 0x4000);
}