#ifndef __TIM4_Driver
#define __TIM4_Driver

#include "Timer.h"
#include "GPIO.h"
#include "RCC.h"

class TIM4_Driver
{
	private:
		static TIMER TIM4;
		static const uint32_t TIM4_BASE;
	
	public:
		TIM4_Driver();
		~TIM4_Driver();
		static void open();
		static void close();
		static void start();
		static void stop();
		
		static void SetPeriod(uint16_t);
	
};

#endif

