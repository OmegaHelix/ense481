#include "GPIO.h"
#include "RCC.h"


class LED_Driver
{
	public:
		enum LED_ {LED_1,LED_2,LED_3,LED_4,LED_5,LED_6,LED_7,LED_8,LED_ALL};
		//Constructor and Destructor
		LED_Driver();
		~LED_Driver();
		//Driver functions
		static void open();
		static void close();
		static void start();
		static void stop();
		
		//Useage Functions
		static void ResetLED(enum LED_Driver::LED_);	
		static void SetLED(enum LED_Driver::LED_);
		static bool StateLED(enum LED_Driver::LED_);
		
		
};

