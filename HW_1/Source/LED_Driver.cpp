#include "LED_Driver.h"

LED_Driver::LED_Driver(){}
LED_Driver::~LED_Driver(){}

	void LED_Driver::ResetLED(LED_Driver::LED_ led)
{
	switch(led)
	{		
		case(LED_1):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b8); 
			break;
		case(LED_2):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b9); 
			break;
		case(LED_3):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b10); 
			break;
		case(LED_4):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b11); 
			break;
		case(LED_5):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b12); 
			break;
		case(LED_6):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b13); 
			break;
		case(LED_7):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b14); 
			break;
		case(LED_8):
			GPIO::Set(GPIO::B, GPIO_PORT::BRR, Register::b15); 
			break;
		case(LED_ALL):
			//Reset all 8 LED bits
			GPIO::Write(GPIO::B, GPIO_PORT::BRR, 0xFF << (8)); 
		break;
	}
} 

void LED_Driver::SetLED(LED_Driver::LED_ led)
{
	switch(led)
	{		
		case(LED_1):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b8); 
			break;
		case(LED_2):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b9); 
			break;
		case(LED_3):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b10); 
			break;
		case(LED_4):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b11); 
			break;
		case(LED_5):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b12); 
			break;
		case(LED_6):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b13); 
			break;
		case(LED_7):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b14); 
			break;
		case(LED_8):
			GPIO::Set(GPIO::B, GPIO_PORT::BSRR, Register::b15); 
			break;
		case(LED_ALL):
			//set all 8 LED bits
			GPIO::Write(GPIO::B, GPIO_PORT::BSRR, 0xFF << (8)); 
		break;
			
	}
} 

bool LED_Driver::StateLED(LED_Driver::LED_ led)
{ bool isOn = false;
	switch(led)
	{		
		case(LED_1):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (1 + 7))); 
			break;
		case(LED_2):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (2 + 7))); 
			break;
		case(LED_3):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (3 + 7))); 
			break;
		case(LED_4):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (4 + 7))); 
			break;
		case(LED_5):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (5 + 7))); 
			break;
		case(LED_6):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (6 + 7))); 
			break;
		case(LED_7):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (7 + 7))); 
			break;
		case(LED_8):
			isOn = static_cast<bool>(GPIO::Read(GPIO::B, GPIO_PORT::IDR) & (0x1 << (8 + 7))); 
			break;
		case(LED_ALL):
		break;
		
	}
	return isOn;
} 





void LED_Driver::open()
{
	// 1. Enable RCC2 GPIOB
	RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPBEN);

	//Wipe the bits for the LED controls
	GPIO::And(GPIO::B, GPIO_PORT::CRH,         ~((0xFu << 0)  | //LED1
                                                 (0xFu << 4)  | //LED2
                                                 (0xFu << 8)  | //LED3
                                                 (0xFu << 12) | //LED4
                                                 (0xFu << 16) | //LED5
                                                 (0xFu << 20) | //LED6
                                                 (0xFu << 24) | //LED7
                                                 (0xFu << 28)));//LED8
	//Set GPIOB  configuration
	GPIO::Or(GPIO::B, GPIO_PORT::CRH,            ((0x3 << 0) | //LED1
                                                 (0x3 << 4)  | //LED2
                                                 (0x3 << 8)  | //LED3
                                                 (0x3 << 12) | //LED4
                                                 (0x3 << 16) | //LED5
                                                 (0x3 << 20) | //LED6
                                                 (0x3 << 24) | //LED7
                                                 (0x3 << 28)));//LED8
	
}

void LED_Driver::close()
{}

void LED_Driver::start()
{
	//enable the GPIO clock
	RCC2::Or(RCC2::APB2ENR, RCC2::_APB2ENR_IOPBEN);
}

void LED_Driver::stop()
{
	RCC2::And(RCC2::APB2ENR, ~RCC2::_APB2ENR_IOPBEN);
}

