#include "USART2_Driver.h"
#include "I2C1_Driver.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "Chev_Trig.h"
#include "Taylor_Trig.h"

//CLI_Driver is a USART2_Driver
class CLI_Driver: public USART2_Driver
{
	private:
	public:
		//Constructor and destructor
		CLI_Driver();
		~CLI_Driver();

		//Driver functions
		void open();
		void close();
		void start();
		void stop();
	
		//Useage functions
		void getLine();
		void sendLine(uint8_t*);
		void sendMenu();
		void newLine();
		void InterperetBuffer();
	
		void getADCReading();
		void getTenReadings();
		void printDoubleCarrot();
		bool PWMBufferComp(uint8_t*);	
		bool bufferComp(uint8_t*);
		void sendHelp();
		void bufferClear();
		void CLI();

		void executeCommand(uint8_t*);
};

