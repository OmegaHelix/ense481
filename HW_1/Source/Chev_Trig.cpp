/**
*
*	_sin_c and _cos_c are renamed _sin and _cos
* functions from Crenshaw Ch5 pp118-119
* to allow multiple implementations in the same project
*	
*	The following functions implement the Chebyshev polynomials
* hence, the postcursor <sin/cos>_c
*
*/

short _sin_c(short y){
	static short s1 = 0x6487;
	static short s3 = 0x2953;
	static short s5 = 0x4F8;
	long z, prod, sum;
	z = ((long)y * y) >> 12;
	prod = (z * s5) >> 16;
	sum = s3 - prod;
	prod = (z * sum) >> 16;
	sum = s1 - prod;
	// for better accuracy, round here
	return (short)((y * sum) >> 13);
}


	short _cos_c(short y){
	static short c0 = 0x7FFF;
	static short c2 = 0x4EE9;
	static short c4 = 0x0FBD;
	long z, prod, sum;
	z = ((long)y * y) >> 12;
		prod = (z * c4) >> 16;
	sum = c2 - prod;
	// for better accuracy, round here
	prod = (z * sum) >> 15;// ; note, not 16
	return (short)(c0 - prod);
}
	

short sin_c(short x){
	short n = (x + 0x2000) >> 14;
	x -= n * 0x4000;
	switch(n){
		case 0:
			return _sin_c(x);
		case 1:
			return _cos_c(x);
		case 2:
			return - _sin_c(x);
		case 3:
			return - _cos_c(x);
		default:
			return 0;
	};
}

short cos_c(short x){
return sin_c(x + 0x4000);
}